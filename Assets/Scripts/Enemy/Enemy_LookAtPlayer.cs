﻿using UnityEngine;
/// <summary>
/// Contacto visual del enemigo cuando el jugador cuando el primero está en modo Attack.
/// </summary>
public class Enemy_LookAtPlayer : Enemy {

    private bool lookAtPlayer = false;

    private new void OnEnable() {
        base.OnEnable();
        enemyMaster.eventPlayerIsInView += OnPlayerIsInView;
        enemyMaster.eventPlayerIsNotLongerInView += OnPlayerIsNoLongerInView;
    }

    private new void OnDisable() {
        base.OnDisable();
        enemyMaster.eventPlayerIsInView -= OnPlayerIsInView;
        enemyMaster.eventPlayerIsNotLongerInView -= OnPlayerIsNoLongerInView;
    }

    void Update() {
        if (lookAtPlayer) {
            Vector3 lookPosition = References.Player.position;
            lookPosition.y = transform.position.y;
            transform.LookAt(lookPosition);
        }
    }

    private void OnPlayerIsInView() {
        lookAtPlayer = true;
    }

    private void OnPlayerIsNoLongerInView() {
        lookAtPlayer = false;
    }

}
