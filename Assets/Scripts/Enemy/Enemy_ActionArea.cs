﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Asociado a un collider. Cuando el jugador entra en contacto con él, simula que ha entrado en su
/// area de acción.
/// </summary>
public class Enemy_ActionArea : Enemy {

    private void OnTriggerEnter(Collider other) {
        if(other.CompareTag(References.Player.tag)) {
            enemyMaster.CallEventPlayerIsNear();
        }
    }

    private void OnTriggerExit(Collider other) {
        if (other.CompareTag(References.Player.tag)) {
            enemyMaster.CallEventPlayerIsFar();
        }
    }
    
}
