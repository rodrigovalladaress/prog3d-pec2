﻿/// <summary>
/// Conector de un GameEvent con un Enemigo. Es utilizado para que el GameEvent recoja los eventos
/// de destrucción del enemigo.
/// </summary>
public class Enemy_GameEventConnector : Enemy {

    private GameEvent_Master gameEventMaster; // Se inicializa en GameEvent_Master

    public GameEvent_Master GameEventMaster {
        get {
            return gameEventMaster;
        }
        set {
            gameEventMaster = value;
        }
    }

    private new void OnEnable() {
        base.OnEnable();
        enemyMaster.eventDie += OnDied;
    }

    private new void OnDisable() {
        base.OnDisable();
        enemyMaster.eventDie -= OnDied;
    }

    private void OnDied() {
        gameEventMaster.CallEventEnemyDestroyed();
    }

}
