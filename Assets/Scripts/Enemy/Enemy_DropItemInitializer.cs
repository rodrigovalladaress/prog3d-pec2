﻿using UnityEngine;
/// <summary>
/// Inicializador del dropeo de ítems de un enemigo en el editor.
/// </summary>
[System.Serializable]
public class Enemy_DropItemInitializer  {

    [SerializeField]
    [Range(0, 1)]
    private float probability;
    [SerializeField]
    private ObjectPoolElement item;

    public float Probability {
        get {
            return probability;
        }
    }

    public ObjectPoolElement Item {
        get {
            return item;
        }
    }
}
