﻿/// <summary>
/// Controlador del enemigo usando una Máquina de Estados Finitos (FSM).
/// </summary>
public class Enemy_Controller : Enemy {

    private DroneFSM fsm;

    private new void OnEnable() {
        base.OnEnable();
        enemyMaster.eventDamaged += OnShootedByPlayer;
        enemyMaster.eventPlayerIsNear += OnPlayerIsNear;
        enemyMaster.eventPlayerIsFar += OnPlayerIsFar;
        enemyMaster.eventStartRotating += OnStartedRotation;
        enemyMaster.eventStopRotating += OnStoppedRotation;
        enemyMaster.eventPlayerIsInView += OnPlayerIsInView;
        enemyMaster.eventPlayerIsNotLongerInView += OnPlayerIsNoLongerInView;
    }

    private new void OnDisable() {
        base.OnDisable();
        enemyMaster.eventDamaged -= OnShootedByPlayer;
        enemyMaster.eventPlayerIsNear -= OnPlayerIsNear;
        enemyMaster.eventPlayerIsFar -= OnPlayerIsFar;
        enemyMaster.eventStartRotating -= OnStartedRotation;
        enemyMaster.eventStopRotating -= OnStoppedRotation;
        enemyMaster.eventPlayerIsInView -= OnPlayerIsInView;
        enemyMaster.eventPlayerIsNotLongerInView -= OnPlayerIsNoLongerInView;
    }

    private new void Awake() {
        base.Awake();
        fsm = new DroneFSM(enemyMaster);
    }

    private void OnShootedByPlayer(int damage) {
        fsm.FsmInfo.shootedByPlayer = true;
        fsm.UpdateState();
    }

    private void OnPlayerIsNear() {
        fsm.FsmInfo.playerInActionRange = true;
        fsm.FsmInfo.shootedByPlayer = false;
        fsm.UpdateState();
    }

    private void OnPlayerIsFar() {
        fsm.FsmInfo.playerInActionRange = false;
        fsm.FsmInfo.shootedByPlayer = false;
        fsm.UpdateState();
    }

    private void OnStartedRotation() {
        fsm.FsmInfo.rotating = true;
    }

    private void OnStoppedRotation() {
        fsm.FsmInfo.playerInView = false;
        fsm.FsmInfo.rotating = false;
        fsm.FsmInfo.shootedByPlayer = false;
        fsm.UpdateState();
    }

    private void OnPlayerIsInView() {
        fsm.FsmInfo.playerInView = true;
        fsm.FsmInfo.shootedByPlayer = false;
        fsm.UpdateState();
    }

    private void OnPlayerIsNoLongerInView() {
        fsm.FsmInfo.playerInView = false;
        fsm.FsmInfo.shootedByPlayer = false;
        fsm.UpdateState();
    }

}
