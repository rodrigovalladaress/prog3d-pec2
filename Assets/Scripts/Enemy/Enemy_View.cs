﻿using UnityEngine;
/// <summary>
/// Simula la visión del enemigo utilizando un raycast desde su punto de visión.
/// </summary>
public class Enemy_View : Enemy {

    [SerializeField]
    private Transform eye;                  // Punto desde el que inicia el raycast.
    [SerializeField]
    private Transform[] viewPoints;         // Puntos a los que apunta el raycast.
    [SerializeField]
    private float rayCastWidth = 2f;

    bool isPlayerInView = false;            // Indica, en general, si se ha encontrado al jugador.
    bool[] eachRayCastView;                 // Indica, para cada raycast, si se ha encontrado al
                                            // jugador.
    bool startedDetection = false;          // Indica si ha comenzado la detección.

    private new void OnEnable() {
        base.OnEnable();
        enemyMaster.eventFSMStateChanged += OnFSMStateChanged;
    }

    private new void OnDisable() {
        base.OnDisable();
        enemyMaster.eventFSMStateChanged -= OnFSMStateChanged;
    }

    private new void Awake() {
        base.Awake();
        eachRayCastView = new bool[viewPoints.Length];
        for (int i = 0; i < eachRayCastView.Length; i++) {
            eachRayCastView[i] = false;
        }
    }

    private void Update() {
        RaycastHit hit;
        Vector3 rayCastOrigin;
        Vector3 rayCastDirection;
        int i;
        rayCastOrigin = eye.transform.position;
        for (i = 0; i < viewPoints.Length; i++) {
            Transform viewPoint = viewPoints[i];
            rayCastDirection = viewPoint.position - eye.transform.position;
            if (Physics.Raycast(new Ray(rayCastOrigin,
            rayCastDirection), out hit)) {
                string hitTag = hit.collider.gameObject.tag;
                if (hitTag == References.Player.tag
                    || Vector3.Distance(hit.point, References.Player.position) <= rayCastWidth) {
                    // Evita llamar el evento continuamente.
                    if (!isPlayerInView || startedDetection) {
                        eachRayCastView[i] = true;
                    }
                }
                else {
                    // Evita llamar el evento continuamente.
                    if (isPlayerInView || startedDetection) {
                        eachRayCastView[i] = false;
                    }
                }
            }
            // Debug del Raycast.
            Debug.DrawRay(rayCastOrigin, rayCastDirection, eachRayCastView[i]
                ? Color.green : Color.red);
        }
        i = 0;
        // Itera por todos los elementos de eachRayCastView y se para en el primero verdadero.
        while (i < viewPoints.Length && !eachRayCastView[i]) {
            i++;
        }
        if (isPlayerInView) {
            // Comprobar si NINGÚN raycast está viendo al jugador.
            if (i == viewPoints.Length) {
                Debug.Log("Enemy_View: object is not player or nothing in view");
                isPlayerInView = false;
                enemyMaster.CallEventPlayerIsNotLongInView();
            }
        }
        else {
            // Comprobar si ALGÚN raycast está viendo al jugador.
            if (i != viewPoints.Length) {
                Debug.Log("Enemy_View: player in view");
                isPlayerInView = true;
                enemyMaster.CallEventPlayerIsInView();
            }
        }
    }

    private void OnFSMStateChanged(FSM.State state, FSM.StateList states) {
        DroneFSM.DroneState droneState = (DroneFSM.DroneState)state;
        DroneFSM.DroneStateList droneStates = (DroneFSM.DroneStateList)states;
        if (droneState == droneStates.Alert || droneState == droneStates.Attack) {
            startedDetection = true;
        }
    }

}
