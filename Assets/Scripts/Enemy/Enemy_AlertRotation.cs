﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Rotación del enemigo cuando está en el estado Alert.
/// </summary>
public class Enemy_AlertRotation : Enemy {

    [SerializeField]
    private float rotationTime = 5f;

    private bool rotate = false;
    float currentRotationTime = 0f;

    private new void OnEnable() {
        base.OnEnable();
        enemyMaster.eventFSMStateChanged += OnFSMStateChanged;
    }

    private new void OnDisable() {
        base.OnDisable();
        enemyMaster.eventFSMStateChanged -= OnFSMStateChanged;
    }

    private void Update() {
        if(rotate) {
            if (currentRotationTime < rotationTime) {
                transform.rotation *= Quaternion.Euler(0f, Time.deltaTime * 360
                    * 1.0f / rotationTime, 0f);
                currentRotationTime += Time.deltaTime;
            }
            else {
                rotate = false;
                enemyMaster.CallEventStoppedRotation();
            }
        }
    }

    private void OnFSMStateChanged(FSM.State state, FSM.StateList states) {
        DroneFSM.DroneState droneState = (DroneFSM.DroneState)state;
        DroneFSM.DroneStateList droneStates = (DroneFSM.DroneStateList)states;
        if (droneState == droneStates.Patrol) {
            rotate = false;
        }
        else if (droneState == droneStates.Alert) {
            rotate = true;
            currentRotationTime = 0f;
            enemyMaster.CallEventStartedRotation();
        }
        else if (droneState == droneStates.Attack) {
            rotate = false;
        }
    }
    

}
