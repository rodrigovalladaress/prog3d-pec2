﻿using UnityEngine;
/// <summary>
/// Cambia el color de la luz del enemigo dependiendo del estado en el que se encuentra.
/// </summary>
public class Enemy_StateLight : Enemy {

    [SerializeField]
    private Light stateLight;
    [SerializeField]
    private Color patrolColor = Color.green;
    [SerializeField]
    private Color alertColor = Color.yellow;
    [SerializeField]
    private Color attackColor = Color.red;

    private new void OnEnable() {
        base.OnEnable();
        enemyMaster.eventFSMStateChanged += OnFSMStateChanged;
    }

    private new void OnDisable() {
        base.OnDisable();
        enemyMaster.eventFSMStateChanged -= OnFSMStateChanged;
    }

    private new void Awake() {
        base.Awake();
        stateLight.color = patrolColor;
    }

    private void OnFSMStateChanged(FSM.State state, FSM.StateList states) {
        DroneFSM.DroneState droneState = (DroneFSM.DroneState)state;
        DroneFSM.DroneStateList droneStates = (DroneFSM.DroneStateList)states;
        if (droneState == droneStates.Patrol) {
            stateLight.color = patrolColor;
        } else if(droneState == droneStates.Alert) {
            stateLight.color = alertColor;
        } else if(droneState == droneStates.Attack) {
            stateLight.color = attackColor;
        }
    }

}
