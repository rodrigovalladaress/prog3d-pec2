﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Clase base de un enemigo.
/// </summary>
public class Enemy : MonoBehaviour {

    private static int PARENT_LIMIT = 5;

    protected Enemy_Master enemyMaster;

    protected void Awake() {
        Initialize();
    }

    protected void OnEnable() {
        Initialize();
        enemyMaster.eventDie += DisableThis;
    }

    protected void OnDisable() {
        enemyMaster.eventDie -= DisableThis;
    }

    private void Initialize() {
        Transform from = transform;
        int checks = 0;
        do {
            enemyMaster = from.GetComponent<Enemy_Master>();
            from = from.parent;
            checks++;
        } while (from && !enemyMaster && checks < PARENT_LIMIT);
    }

    protected void DisableThis() {
        Destroy(gameObject);
    }

}
