﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Disparo de un enemigo.
/// </summary>
public class Enemy_Fire : Enemy {

    [SerializeField]
    private float fireRateRPS = 10f;                            // Número de disparos por segundo.
    [SerializeField]
    [Range(Weapon_Info.MIN_DAMAGE, Weapon_Info.MAX_DAMAGE)]
    private int damage = 10;                                    // Daño que inflige al jugador.
    [SerializeField]
    private float firstShotDelay = 0.2f;                        // Tiempo de espera del enemigo
                                                                // para efectuar el primer disparo.

    private bool playerInView;              // ¿El jugador está en el rango de visión?
    private bool canFire = true;            // Flag que indica si se puede disparar en este frame.
    private float secondsBetweenShots;      // Tiempo de espera entre cada disparo.

    private new void Awake() {
        base.Awake();
        secondsBetweenShots = 1 / fireRateRPS;
    }

    private new void OnEnable() {
        base.OnEnable();
        enemyMaster.eventPlayerIsInView += OnPlayerIsInView;
        enemyMaster.eventPlayerIsNotLongerInView += OnPlayerIsNoLongerInView;
    }

    private new void OnDisable() {
        enemyMaster.eventPlayerIsInView += OnPlayerIsInView;
        enemyMaster.eventPlayerIsNotLongerInView += OnPlayerIsNoLongerInView;
    }

    void Update () {
		if(playerInView && canFire) {
            StartCoroutine(WaitForNextFire(secondsBetweenShots));
            enemyMaster.CallEventDamagePlayer(damage);
            enemyMaster.CallEventShooted();
        }
	}

    private void OnPlayerIsInView() {
        StartCoroutine(WaitForNextFire(firstShotDelay));
        playerInView = true;
    }

    private void OnPlayerIsNoLongerInView() {
        playerInView = false;
    }

    private IEnumerator WaitForNextFire(float seconds) {
        canFire = false;
        yield return new WaitForSeconds(secondsBetweenShots);
        canFire = true;
    }

}
