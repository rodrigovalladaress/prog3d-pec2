﻿using UnityEngine;
/// <summary>
/// A partir de un Enemy_DropItemInitializer, se crea un objeto Enemy_DropItemRate, que va a utili-
/// zarse internamente en Enemy_DropItem.
/// </summary>
public class Enemy_DropItemRate {

    private float startRate;
    private float endRate;
    private int itemPos;

    public float StartRate {
        get {
            return startRate;
        }
    }

    public float EndRate {
        get {
            return endRate;
        }
    }

    public int ItemPoolPos {
        get {
            return itemPos;
        }
    }

    public Enemy_DropItemRate(Enemy_DropItemInitializer initializer, float accumulatedRate) {
        Initialize(initializer, accumulatedRate);
    }

    private void Initialize(Enemy_DropItemInitializer initializer, float accumulatedRate) {
        startRate = accumulatedRate;
        endRate = accumulatedRate + initializer.Probability;
        itemPos = References.ObjectPool
            .GetPoolPosition(initializer
            .Item
            .name);
        if(endRate > 1f) {
            Debug.LogWarning("Endrate of " + initializer.Item + "(objectPool position = " 
                + ItemPoolPos + ")" + endRate);
            endRate = 1f;
        }
    }

}
