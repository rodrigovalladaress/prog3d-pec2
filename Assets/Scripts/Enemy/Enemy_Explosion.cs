﻿using UnityEngine;
/// <summary>
/// Explosión del enemigo cuando este es derrotado.
/// </summary>
public class Enemy_Explosion : Enemy {

    [SerializeField]
    private ObjectPoolElement explosionPrefab;
    [SerializeField]
    private Transform explosionPoint;

    int explosionPoolPos;

    private void Start() {
        explosionPoolPos = References.ObjectPool.GetPoolPosition(explosionPrefab.name);
    }

    protected new void OnEnable() {
        base.OnEnable();
        enemyMaster.eventDie += InstantiateExplosion;
    }

    protected new void OnDisable() {
        base.OnDisable();
        enemyMaster.eventDie -= InstantiateExplosion;
    }

    private void InstantiateExplosion() {
        References.ObjectPool.Request(explosionPoolPos, explosionPoint.position, 
            Quaternion.identity);
    }

}
