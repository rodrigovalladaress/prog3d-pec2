﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Sonido del enemigo cuando este dispara.
/// </summary>
public class Enemy_Sound : Enemy {

    private AudioSource audioSource;

    private new void Awake() {
        audioSource = GetComponent<AudioSource>();
    }

    private new void OnEnable() {
        base.OnEnable();
        enemyMaster.eventShooted += OnShooted;
    }

    private new void OnDisable() {
        base.OnDisable();
        enemyMaster.eventShooted -= OnShooted;
    }

    private void OnShooted() {
        audioSource.Play();
    }

}
