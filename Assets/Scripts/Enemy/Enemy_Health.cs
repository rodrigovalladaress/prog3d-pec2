﻿using UnityEngine;
/// <summary>
/// Vida de un enemigo.
/// </summary>
public class Enemy_Health : Enemy {

    public const int MAX_HEALTH = 100;

    [SerializeField]
    [Range(0, MAX_HEALTH)]
    private int initialHealth = 100;

    private int health;

    private new void Awake() {
        base.Awake();
        health = initialHealth;
    }

    private new void OnEnable() {
        base.OnEnable();
        enemyMaster.eventDamaged += HarmEnemy;
    }

    private new void OnDisable() {
        base.OnDisable();
        enemyMaster.eventDamaged += HarmEnemy;
    }

    private void HarmEnemy(int damage) {
        health -= damage;
        if(health < 0) {
            health = 0;
        }
        if(health == 0) {
            enemyMaster.CallEventDie();
        }
    }

}
