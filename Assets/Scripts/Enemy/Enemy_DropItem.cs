﻿using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Cuando el enemigo es destruido, suelta un ítem.
/// </summary>
public class Enemy_DropItem : Enemy {

    [SerializeField]
    private List<Enemy_DropItemInitializer> itemDropRatesInitializer;   // En el editor se inicia-
                                            // liza el porcentaje de dropeo de cada ítem.
    [SerializeField]
    private Transform dropItemPoint;        // Punto en el que aparece el ítem dropeado.

    private List<Enemy_DropItemRate> itemDropRates; // Porcentaje interno de dropeo de cada ítem.

    private new void Awake() {
        base.Awake();
        itemDropRates = new List<Enemy_DropItemRate>();
    }

    private void Start() {
        float accumulatedDropRate = 0f;
        foreach (Enemy_DropItemInitializer initializer in itemDropRatesInitializer) {
            Enemy_DropItemRate itemDropRate = new Enemy_DropItemRate(initializer,
                accumulatedDropRate);
            itemDropRates.Add(itemDropRate);
            accumulatedDropRate += itemDropRate.EndRate;
        }
    }

    private new void OnEnable() {
        base.OnEnable();
        enemyMaster.eventDie += OnEnemyDied;
    }

    private new void OnDisable() {
        base.OnDisable();
        enemyMaster.eventDie += OnEnemyDied;
    }

    private void OnEnemyDied() {
        float randomRate = Random.Range(0f, 1f);
        int i = 0;
        Enemy_DropItemRate itemDropRate = null;
        while (i < itemDropRates.Count &&
            !((itemDropRate = itemDropRates[i]).StartRate <= randomRate
            && itemDropRate.EndRate > randomRate)) {
            i++;
        }
        if (i != itemDropRates.Count) {
            References.ObjectPool.Request(itemDropRate.ItemPoolPos, dropItemPoint.position,
                Quaternion.identity);
        }
    }

}
