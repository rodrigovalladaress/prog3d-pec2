﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Partícula que se muestra cuando el enemigo dispara.
/// </summary>
public class Enemy_ParticleSystem : Enemy {

    [SerializeField]
    private ParticleSystem shotParticle;

    private new void OnEnable() {
        base.OnEnable();
        enemyMaster.eventShooted += OnShooted;
    }

    private new void OnDisable() {
        base.OnDisable();
        enemyMaster.eventShooted -= OnShooted;
    }

    private void OnShooted() {
        shotParticle.Clear();
        shotParticle.Play();
    }

}
