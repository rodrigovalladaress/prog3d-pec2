﻿using UnityEngine;
using UnityEngine.AI;
/// <summary>
/// Permite al enemigo seguir unos waypoints dados en el editor para simular que patrulla.
/// </summary>
public class Enemy_FollowWaypoints : Enemy {

    public const float EPSILON = 0.1f;

    [SerializeField]
    private Transform center;                               // Centro del renderer.

    [SerializeField]
    private Transform[] waypoints;                          // Puntos de patrulla.

    private NavMeshAgent agent;
    private int wIterator = 0;                              // Iterador de waypoints.

    private new void OnEnable() {
        base.OnEnable();
        enemyMaster.eventSetPatrolTarget += SetPatrolTarget;
        enemyMaster.eventTargetReached += OnTargetReached;
        enemyMaster.eventFSMStateChanged += OnFSMStateChanged;
        if(waypoints.Length > 0) {
            enemyMaster.CallEventSetPatrolTarget(waypoints[wIterator]);
        }
    }

    private new void OnDisable() {
        base.OnDisable();
        enemyMaster.eventSetPatrolTarget -= SetPatrolTarget;
        enemyMaster.eventTargetReached -= OnTargetReached;
        enemyMaster.eventFSMStateChanged -= OnFSMStateChanged;
    }

    private new void Awake() {
        base.Awake();
        agent = GetComponent<NavMeshAgent>();
    }

    private void OnFSMStateChanged(FSM.State state, FSM.StateList states) {
        DroneFSM.DroneState droneState = (DroneFSM.DroneState)state;
        DroneFSM.DroneStateList droneStates = (DroneFSM.DroneStateList)states;
        if (droneState == droneStates.Patrol) {
            agent.Resume();
        }
        else if (droneState == droneStates.Alert) {
            agent.Stop();
        }
        else if (droneState == droneStates.Attack) {
            agent.Stop();
        }
    }

    private void OnTargetReached() {
        if(waypoints.Length > 0) {
            wIterator++;
            wIterator %= waypoints.Length;
            enemyMaster.CallEventSetPatrolTarget(waypoints[wIterator]);
        }
    }

    private void SetPatrolTarget(Transform target) {
        agent.destination = target.position;
    }

    void Update () {
        if (PECUtilities.Near(new Vector3(center.position.x, agent.destination.y, center.position.z),
                agent.destination, EPSILON)) {
            enemyMaster.CallEventTargetReached();
        }
    }
    
}
