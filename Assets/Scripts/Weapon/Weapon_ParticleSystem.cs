﻿using UnityEngine;
/// <summary>
/// Animación del arma cuando se dispara.
/// </summary>
public class Weapon_ParticleSystem : Weapon {

    [SerializeField]
    private ParticleSystem shotParticle;

    private new void OnEnable() {
        base.OnEnable();
        weaponMaster.eventSuccesfullyFiredWeapon += OnSuccesfullyFiredWeapon;
    }

    private new void OnDisable() {
        base.OnEnable();
        weaponMaster.eventSuccesfullyFiredWeapon -= OnSuccesfullyFiredWeapon;
    }

    private void OnSuccesfullyFiredWeapon() {
        shotParticle.Clear();
        shotParticle.Play();
    }

}
