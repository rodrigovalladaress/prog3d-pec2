﻿using UnityEngine;
/// <summary>
/// Clase base para lo relacionado con las armas del jugador.
/// </summary>
public class Weapon : MonoBehaviour {

    private static int PARENT_LIMIT = 5;

    protected Weapon_Master weaponMaster;

    protected void Awake() {
        Initialize();
    }

    protected void OnEnable() {
        //weaponMaster.eventEnemyDie += DisableThis;
    }

    protected void OnDisable() {
        //weaponMaster.eventEnemyDie -= DisableThis;
    }

    private void Initialize() {
        Transform from = transform;
        int checks = 0;
        do {
            weaponMaster = from.GetComponent<Weapon_Master>();
            from = from.parent;
            checks++;
        } while (from && !weaponMaster && checks < PARENT_LIMIT);
    }

    protected void DisableThis() {
        enabled = false;
    }
}
