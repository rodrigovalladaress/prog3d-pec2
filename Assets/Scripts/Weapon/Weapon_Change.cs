﻿/// <summary>
/// Permite cambiar el arma del jugador.
/// </summary>
public class Weapon_Change : Weapon {

    private Weapon_Info[] weaponList;            // Lista de armas que puede llevar el jugador
    private Weapon_Info activeWeapon;            // Arma activa actualmente.

    private new void Awake() {
        base.Awake();
    }

    private void Start() {
        weaponList = References.WeaponList;
        InitializeActiveWeapon();
    }

    private new void OnEnable() {
        base.OnEnable();
        weaponMaster.eventChangeWeapon += OnChangeWeapon;
    }

    private new void OnDisable() {
        base.OnDisable();
        weaponMaster.eventChangeWeapon -= OnChangeWeapon;
    }

    private void OnChangeWeapon(Weapon_Info.WeaponIdEnum weaponId) {
        SetWeaponActive(activeWeapon, false);
        SetWeaponActive(GetWeaponById(weaponId), true);
    }

    // Busca entre la lista de armas una que tenga su GameObject activo, que se convertirá en el 
    // arma activa. Si no encuentra ninguna, la primera arma de la lista será la activa.
    private void InitializeActiveWeapon() {
        int i = 0;
        while (i < weaponList.Length && !weaponList[i].gameObject.activeInHierarchy) {
            i++;
        }
        if (i != weaponList.Length && weaponList.Length != 0) {
            SetWeaponActive(weaponList[i], true);
            // Desactiva las demás armas por si hubiera más de una activa en el editor.
            for(int j = 0; j < weaponList.Length; j++) {
                if(j != i) {
                    SetWeaponActive(weaponList[j], false);
                }
            }
        }
    }

    private void SetWeaponActive(Weapon_Info weapon, bool active) {
        weapon.gameObject.SetActive(active);
        if(active) {
            activeWeapon = weapon;
            weaponMaster.CallEventSuccesfullyChangedWeapon(weapon);
        }
    }

    private Weapon_Info GetWeaponById(Weapon_Info.WeaponIdEnum weaponId) {
        int i = 0;
        Weapon_Info r = null;
        while(i < weaponList.Length && weaponList[i].WeaponId != weaponId) {
            i++;
        }
        if(i != weaponList.Length) {
            r = weaponList[i];
        }
        return r;
    }

}
