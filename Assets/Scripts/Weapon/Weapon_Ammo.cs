﻿/// <summary>
/// Gestión de los cambios de munición del arma.
/// </summary>
public class Weapon_Ammo : Weapon {

    private Weapon_Info weaponInfo;
    private int ammo = 0;

    public int Ammo {
        get {
            return ammo;
        }
        set {
            int newAmmo = value;
            if (newAmmo < 0) {
                newAmmo = 0;
            }
            if (newAmmo > weaponInfo.MaxAmmo) {
                newAmmo = weaponInfo.MaxAmmo;
            }
            if (ammo == 0 && newAmmo > 0) {
                weaponMaster.CallEventEnoughAmmoForFiring(true);
            }
            if (ammo > 0 && newAmmo == 0) {
                weaponMaster.CallEventEnoughAmmoForFiring(false);
            }
            ammo = newAmmo;
            // Llamar al evento AmmoChanged solo si esta es el arma activa, ya que el jugador puede
            // recoger munición de un arma que no tenga ctiva en ese momento.
            if (enabled) {
                weaponMaster.CallEventAmmoChanged(ammo);
            }
        }
    }

    private new void OnEnable() {
        base.OnEnable();
        weaponMaster.eventSuccesfullyChangedWeapon += OnSuccesfullyChangedWeapon;
        weaponMaster.eventReduceAmmoBy += OnReduceAmmoBy;
    }

    private new void OnDisable() {
        base.OnDisable();
        weaponMaster.eventSuccesfullyChangedWeapon -= OnSuccesfullyChangedWeapon;
        weaponMaster.eventReduceAmmoBy -= OnReduceAmmoBy;
    }

    private new void Awake() {
        base.Awake();
        weaponInfo = GetComponent<Weapon_Info>();
        ammo = weaponInfo.MaxAmmo;
    }

    private void OnSuccesfullyChangedWeapon(Weapon_Info weapon) {
        bool enoughAmmo = true;
        if(ammo == 0) {
            enoughAmmo = false;
        }
        weaponMaster.CallEventEnoughAmmoForFiring(enoughAmmo);
        weaponMaster.CallEventAmmoChanged(ammo);
    }

    private void OnReduceAmmoBy(int deltaAmmo) {
        Ammo -= deltaAmmo;
    }

    // Este método se llama desde Weapon_ItemHandler, no desde un evento.
    public void AmmoItemObtained(int deltaAmmo) {
        Ammo += deltaAmmo;
    }

}
