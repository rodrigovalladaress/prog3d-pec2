﻿using UnityEngine;
/// <summary>
/// Sonido de disparo del arma.
/// </summary>
public class Weapon_Sound : Weapon {

    private AudioSource audioSource;

    private new void Awake() {
        base.Awake();
        audioSource = GetComponent<AudioSource>();
    }

    private new void OnEnable() {
        base.OnEnable();
        weaponMaster.eventSuccesfullyFiredWeapon += OnSuccesfullyFired;
    }

    private new void OnDisable() {
        base.OnDisable();
        weaponMaster.eventSuccesfullyFiredWeapon -= OnSuccesfullyFired;
    }

    private void OnSuccesfullyFired() {
        audioSource.Play();
    }

}
