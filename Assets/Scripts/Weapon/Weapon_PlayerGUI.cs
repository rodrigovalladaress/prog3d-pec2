﻿/// <summary>
/// Gestión de las llamadas a PlayerGUI para actualizar su estado cuando cambia algún dato del 
/// arma.
/// </summary>
public class Weapon_PlayerGUI : Weapon {

    private Weapon_Info weaponInfo;

    private new void OnEnable() {
        base.OnEnable();
        weaponMaster.eventSuccesfullyChangedWeapon += OnSuccesfullyChangedWeapon;
        weaponMaster.eventAmmoChanged += OnAmmoChanged;
    }

    private new void OnDisable() {
        base.OnDisable();
        weaponMaster.eventSuccesfullyChangedWeapon -= OnSuccesfullyChangedWeapon;
        weaponMaster.eventAmmoChanged -= OnAmmoChanged;
    }

    private new void Awake() {
        base.Awake();
        weaponInfo = GetComponent<Weapon_Info>();
    }

    private void OnSuccesfullyChangedWeapon(Weapon_Info weapon) {
        weaponMaster.CallEventUpdateWeaponNameGUI(weapon.WeaponName);
    }

    private void OnAmmoChanged(int newAmmo) {
        weaponMaster.CallEventUpdateAmmoGUI(newAmmo, weaponInfo.MaxAmmo);
    }

}
