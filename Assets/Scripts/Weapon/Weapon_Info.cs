﻿using UnityEngine;
/// <summary>
/// Información del arma.
/// </summary>
public class Weapon_Info : MonoBehaviour {

    public enum WeaponIdEnum {
        Pistol,
        MachineGun
    }

    public const float MIN_STABILITY = 0f;
    public const float MAX_STABILITY = 10f;
    public const int MIN_DAMAGE = 1;
    public const int MAX_DAMAGE = 100;
    public const float FIRE_ERROR_MULTIPLIER = 0.7f;
    public const int MAX_AMMO = 300;

    [SerializeField]
    private WeaponIdEnum weaponId;
    [SerializeField]
    private string weaponName;
    [SerializeField]
    private float range = 25f;              // Alcance del arma.
    [SerializeField]
    private float fireRateRPS = 10f;        // Número de disparos por segundo.
    [SerializeField]
    [Range(MIN_STABILITY, MAX_STABILITY)]
    private float stability = 10f;          // Estabilidad del arma. Cuanto más estable sea, menos
                                            // errores de disparo tendrá.
    [SerializeField]
    [Range(0, MAX_AMMO)]
    private int maxAmmo = 300;
    [SerializeField]
    [Range(MIN_DAMAGE, MAX_DAMAGE)]
    private int damage = 10;
    [SerializeField]
    private GameObject bulletHolePrefab;

    public WeaponIdEnum WeaponId {
        get {
            return weaponId;
        }
    }

    public string WeaponName {
        get {
            return weaponName;
        }
    }

    public float Range {
        get {
            return range;
        }
    }

    public float FireRateRPS {
        get {
            return fireRateRPS;
        }
    }

    public float Stability {
        get {
            return stability;
        }
    }

    public int MaxAmmo {
        get {
            return maxAmmo;
        }
    }

    public GameObject BulletHolePrefab {
        get {
            return bulletHolePrefab;
        }
    }

    public int Damage {
        get {
            return damage;
        }
    }
}
