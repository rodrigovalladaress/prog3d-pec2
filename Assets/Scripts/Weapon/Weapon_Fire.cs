﻿using System.Collections;
using UnityEngine;
/// <summary>
/// Disparo del arma.
/// </summary>
public class Weapon_Fire : Weapon {

    [SerializeField]
    private LayerMask rayCastLayer;

    private int poolPos;                    // Posición del prefab del agujero de bala en el 
                                            // ObjectPool.
    private bool canFire;                   // Flag que indica si se puede disparar en este frame.
    private float waitSeconds;              // Tiempo de espera entre cada disparo.
    private float fireError;                // Error que se aplica al disparo según la estabilidad
                                            // del arma (porcentaje).
    private bool enoughAmmo;                // ¿Hay suficiente munición para disparar el arma?
    private Weapon_Info weaponInfo;

    private new void OnEnable() {
        base.OnEnable();
        weaponMaster.eventRequestWeaponFire += Fire;
        weaponMaster.eventEnoughAmmoForFiring += SetEnoughAmmoForFiring;
        canFire = true;
    }

    private new void OnDisable() {
        base.OnDisable();
        weaponMaster.eventRequestWeaponFire -= Fire;
        weaponMaster.eventEnoughAmmoForFiring -= SetEnoughAmmoForFiring;
    }

    private new void Awake() {
        base.Awake();
        weaponInfo = GetComponent<Weapon_Info>();
        waitSeconds = 1 / weaponInfo.FireRateRPS;
        fireError = (1 - (weaponInfo.Stability /
            (Weapon_Info.MAX_STABILITY - Weapon_Info.MIN_STABILITY)))
            * Weapon_Info.FIRE_ERROR_MULTIPLIER;
    }

    private void Start() {
        poolPos = References.ObjectPool.GetPoolPosition(weaponInfo.BulletHolePrefab.name);
    }

    private void Fire() {
        if (canFire && enoughAmmo) {
            RaycastHit initialHit;
            RaycastHit hit;
            float distanceErrorPercentage;
            weaponMaster.CallEventSuccesfullyFiredWeapon();
            // Primero se realiza un Raycast al punto de disparo "ideal" (aquel si el arma tuviera
            // máxima estabilidad).
            if (Physics.Raycast(Camera.main.transform.TransformPoint(Vector3.zero),
                Camera.main.transform.forward, out initialHit, weaponInfo.Range,
                rayCastLayer.value)) {
                // Con el punto de disparo ideal, se obtiene el porcentaje de error debido a la 
                // distancia del objetivo al que se está disparando.
                distanceErrorPercentage = Vector3.Distance(transform.position, initialHit.point)
                    / weaponInfo.Range;
                // Obtenido el error relativo a la distancia, se calcula el punto final de disparo,
                // teniendo en cuenta la imprecisión debida a la distancia del objetivo y a la
                // propia estabilidad del arma.
                if (Physics.Raycast(Camera.main.transform.TransformPoint(
                    CreateRandomPoint(distanceErrorPercentage)),
                    Camera.main.transform.forward, out hit, weaponInfo.Range, rayCastLayer.value)) {
                    // Si el punto final de disparo se encuentra dentro del rango del arma, se ha
                    // alcanzado al objetivo con una bala.
                    Enemy_Master enemy = hit.collider.GetComponentInParent<Enemy_Master>();
                    weaponMaster.CallEventReduceAmmoBy(1);
                    if (enemy) {
                        enemy.CallEventEnemyDamaged(weaponInfo.Damage);
                    }
                    else {
                        References.ObjectPool.Request(poolPos, hit.point + hit.normal * 0.01f,
                        Quaternion.FromToRotation(Vector3.forward, -hit.normal), hit.transform);
                    }
                }
                else {
                    //Debug.Log("No impacto");
                }
            }
            StartCoroutine(WaitForNextFire());
        }
    }

    private void SetEnoughAmmoForFiring(bool enoughAmmo) {
        this.enoughAmmo = enoughAmmo;
    }

    private Vector3 CreateRandomPoint(float distanceErrorPercentage) {
        float currentFireError = distanceErrorPercentage * fireError;
        float randomX = Random.Range(-currentFireError, currentFireError);
        float randomY = Random.Range(-currentFireError, currentFireError);
        float randomZ = Random.Range(-currentFireError, currentFireError);
        return new Vector3(randomX, randomY, randomZ);
    }

    private IEnumerator WaitForNextFire() {
        canFire = false;
        yield return new WaitForSeconds(waitSeconds);
        canFire = true;
    }

}
