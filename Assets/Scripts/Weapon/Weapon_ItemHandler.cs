﻿using System.Collections.Generic;
/// <summary>
/// Gestión del cambio de munición de un arma cuando el jugador recoge un ítem de munición.
/// </summary>
public class Weapon_ItemHandler : Weapon {

    Dictionary<Weapon_Info.WeaponIdEnum, Weapon_Ammo> weaponAmmoDc;

    private void Start() {
        Weapon_Info[] weaponList = References.WeaponList;
        weaponAmmoDc = new Dictionary<Weapon_Info.WeaponIdEnum, Weapon_Ammo>();
        foreach (Weapon_Info w in weaponList) {
            weaponAmmoDc.Add(w.WeaponId, w.GetComponent<Weapon_Ammo>());
        }
    }

    private new void OnEnable() {
        base.OnEnable();
        weaponMaster.eventAmmoItemObtained += OnAmmoItemObtained;
    }

    private new void OnDisable() {
        base.OnEnable();
        weaponMaster.eventAmmoItemObtained -= OnAmmoItemObtained;
    }

    private void OnAmmoItemObtained(Weapon_Info.WeaponIdEnum weaponId, int deltaAmmo) {
        weaponAmmoDc[weaponId].AmmoItemObtained(deltaAmmo);
    }

}
