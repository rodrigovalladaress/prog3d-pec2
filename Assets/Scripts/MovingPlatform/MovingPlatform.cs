﻿using UnityEngine;
/// <summary>
/// Clase base de todas las relacionadas con plataformas móviles.
/// </summary>
public class MovingPlatform : MonoBehaviour {

    private static int PARENT_LIMIT = 5;

    protected MovingPlatform_Master movingPlatformMaster;

    protected void Awake() {
        Initialize();
    }

    protected void OnEnable() {
        movingPlatformMaster.eventDestroy += DisableThis;
    }

    protected void OnDisable() {
        movingPlatformMaster.eventDestroy -= DisableThis;
    }

    private void Initialize() {
        Transform from = transform;
        int checks = 0;
        do {
            movingPlatformMaster = from.GetComponent<MovingPlatform_Master>();
            from = from.parent;
            checks++;
        } while (from && !movingPlatformMaster && checks < PARENT_LIMIT);
    }

    protected void DisableThis() {
        Destroy(gameObject);
    }

}
