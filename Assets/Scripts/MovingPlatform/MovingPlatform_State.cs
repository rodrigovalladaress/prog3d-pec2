﻿using UnityEngine;
/// <summary>
/// Estado de la plataforma.
/// </summary>
public class MovingPlatform_State : MovingPlatform {

    public enum PlatformStateEnum {
        Locked,     // La plataforma no se moverá
        Automatic,  // La plataforma se moverá si el jugador está encima
        Unlocked    // La plataforma sin necesidad de interacción del jugador
    }

    [SerializeField]
    private PlatformStateEnum state;

    public PlatformStateEnum State {
        get {
            return state;
        }

        set {
            state = value;
            movingPlatformMaster.CallEventPlatformStateChanged(state);
        }
    }

    void Start () {
        movingPlatformMaster.CallEventPlatformStateChanged(state);
    }
}
