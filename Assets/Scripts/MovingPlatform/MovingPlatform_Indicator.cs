﻿using UnityEngine;
/// <summary>
/// Modifica el aspecto del hexágono interior de la plataforma para indicar el estado en el que se 
/// encuentra (el de MovingPlatform_State).
/// </summary>
public class MovingPlatform_Indicator : MovingPlatform {

    [SerializeField]
    private Renderer indicatorRenderer;
    [SerializeField]
    private Material lockedMaterial;
    [SerializeField]
    private Material unlockedMaterial;
    [SerializeField]
    private Material automaticMaterial;

    private new void OnEnable() {
        base.OnEnable();
        movingPlatformMaster.eventPlatformStateChanged += OnPlatformStateChanged;        
    }

    private new void OnDisable() {
        base.OnDisable();
        movingPlatformMaster.eventPlatformStateChanged -= OnPlatformStateChanged;
    }

    private void OnPlatformStateChanged(MovingPlatform_State.PlatformStateEnum platformState) {
        switch (platformState) {
            case MovingPlatform_State.PlatformStateEnum.Locked:
                indicatorRenderer.material = lockedMaterial;
                break;
            case MovingPlatform_State.PlatformStateEnum.Automatic:
                indicatorRenderer.material = automaticMaterial;
                break;
            case MovingPlatform_State.PlatformStateEnum.Unlocked:
                indicatorRenderer.material = unlockedMaterial;
                break;
            default:
                Debug.LogWarning("State " + platformState.ToString() + " doesn't exists!");
                break;
        }
    }
    
}
