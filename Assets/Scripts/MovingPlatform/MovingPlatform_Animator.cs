﻿using System.Collections;
using UnityEngine;
/// <summary>
/// Interpola las posiciones y rotaciones de la plataforma cuando esta se encuentra en movimiento.
/// </summary>
public class MovingPlatform_Animator : MovingPlatform {

    public const float EPSILON = 0.1f;
    public const float MIN_SPEED = 0.01f;

    [SerializeField]
    private Transform platform;
    [SerializeField]
    private Transform[] positions;              // Lista de posiciones y rotaciones por las que
                                                // pasa la plataforma.
    [SerializeField]
    private float speed = 1f;                   // Velocidad de desplazamiento de la plataforma.
    [SerializeField]
    private float waitTime = 1.5f;              // Tiempo que espera la plataforma entre cada 
                                                // posición de su recorrido.

    private bool canMove = false;               // ¿La plataforma puede moverse o está bloqueada?
    private int pIterator = 0;                  // Iterador de posiciones
    private Vector3 startedMovedPos;            // Posición en la que empezó a moverse.
    //private Quaternion startedMovedRot;         // Rotación en la que empezó a moverse.
    private float distanceToNextPosition;
    private float accumulatedDeltaTime = 0f;    // Tiempo acumulado en un movimiento.
    private float interpolationTime;
    private bool waiting = false;
    private bool automaticPlatform = false;
    private bool playerIsOnPlatform = false;

    private new void Awake() {
        base.Awake();
        pIterator = 0;
        InitializeNextPosition();
        if(speed <= 0f) {
            speed = MIN_SPEED;
        }
    }

    private new void OnEnable() {
        base.OnEnable();
        movingPlatformMaster.eventPositionReached += OnPositionReached;
        movingPlatformMaster.eventPlayerOnPlatform += OnPlayerOnPlatform;
        movingPlatformMaster.eventPlatformStateChanged += OnPlatformStateChanged;
    }

    private new void OnDisable() {
        base.OnDisable();
        movingPlatformMaster.eventPositionReached -= OnPositionReached;
        movingPlatformMaster.eventPlayerOnPlatform -= OnPlayerOnPlatform;
        movingPlatformMaster.eventPlatformStateChanged -= OnPlatformStateChanged;
    }

    void FixedUpdate() {
        if (canMove && !waiting) {
            int pIteratorPrev = (pIterator - 1) >= 0 ? pIterator - 1 : positions.Length - 1;
            Vector3 startPoint = positions[pIteratorPrev].position;
            Vector3 endPoint = positions[pIterator].position;
            Quaternion startRot = positions[pIteratorPrev].rotation;
            Quaternion endRot = positions[pIterator].rotation;
            float distanceAlreadyMoved;
            float percentage;
            float percentageAlreadyCompleted;
            float secondsAlreadyPassed;
            if(distanceToNextPosition > 0) {
                accumulatedDeltaTime += Time.fixedDeltaTime;
                distanceAlreadyMoved = (startedMovedPos - startPoint).magnitude;
                percentageAlreadyCompleted = distanceAlreadyMoved / distanceToNextPosition;
                secondsAlreadyPassed = percentageAlreadyCompleted * interpolationTime;
                percentage = (accumulatedDeltaTime + secondsAlreadyPassed) / interpolationTime;
            } else {
                percentage = 1f;
            }
            if (percentage >= 1f) {
                movingPlatformMaster.CallEventPositionReached();
            }
            else {
                platform.transform.position = Vector3.Slerp(startPoint,
                    endPoint, percentage);
                platform.transform.rotation = Quaternion.Slerp(startRot, endRot, percentage);
            }
        }
    }

    private void OnPlatformStateChanged(MovingPlatform_State.PlatformStateEnum platformState) {
        if(platformState == MovingPlatform_State.PlatformStateEnum.Unlocked) {
            canMove = true;
            automaticPlatform = false;
        } else if(platformState == MovingPlatform_State.PlatformStateEnum.Automatic) {
            canMove = false;
            automaticPlatform = true;
        } else if(platformState == MovingPlatform_State.PlatformStateEnum.Locked) {
            canMove = false;
            automaticPlatform = false;
        }
    }

    private void OnPlayerOnPlatform(bool playerIsOnPlatform) {
        this.playerIsOnPlatform = playerIsOnPlatform;
        if(automaticPlatform) {
            if (this.playerIsOnPlatform) {
                if (!canMove) {
                    canMove = true;
                }
            }
        }
    }

    private void OnPositionReached() {
        if (positions.Length > 0) {
            pIterator++;
            pIterator %= positions.Length;
            InitializeNextPosition();
            StartCoroutine(PlatformWait());
            if(automaticPlatform && !playerIsOnPlatform) {
                if (pIterator == 0) {
                    canMove = true;
                } else {
                    canMove = false;
                }
            }
        }
    }

    private void InitializeNextPosition() {
        if (positions.Length > 0) {
            Vector3 nextPosition = positions[pIterator].position;
            Vector3 vDistance = (nextPosition - platform.position);
            distanceToNextPosition = vDistance.magnitude;
            interpolationTime = distanceToNextPosition / speed;
            startedMovedPos = platform.position;
            //startedMovedRot = platform.rotation;
            accumulatedDeltaTime = 0f;
        }
    }

    private IEnumerator PlatformWait() {
        waiting = true;
        yield return new WaitForSeconds(waitTime);
        waiting = false;
    }

}
