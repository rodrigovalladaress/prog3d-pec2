﻿using UnityEngine;
/// <summary>
///  Cambia el parent de Player a la propia plataforma. Esto asegura que, cuando el jugador se
///  encuentre encima de la plataforma, este no se caiga, sino que se mueva con ella.
/// </summary>
public class MovingPlatform_HoldPlayer : MovingPlatform {

    private Transform originalParent = null;
    private bool alreadyOnPlatform = false;

    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag(References.PlayerTag) && !alreadyOnPlatform) {
            alreadyOnPlatform = true;
            Quaternion originalRot = other.transform.rotation;
            originalParent = other.transform.parent;
            other.transform.parent = transform;
            movingPlatformMaster.CallEventPlayerOnPlatform(true);
        }
    }

    private void OnTriggerExit(Collider other) {
        if (other.CompareTag(References.PlayerTag) && alreadyOnPlatform) {
            other.transform.parent = originalParent;
            movingPlatformMaster.CallEventPlayerOnPlatform(false);
            alreadyOnPlatform = false;
        }
    }

}
