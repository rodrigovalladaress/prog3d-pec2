﻿using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
/// <summary>
/// Juego completado.
/// </summary>
public class Player_GameCompleted : Player {

    private FirstPersonController firstPersonController;

    private new void Awake() {
        base.Awake();
        firstPersonController = GetComponent<FirstPersonController>();
    }

    private new void OnEnable() {
        base.OnEnable();
        playerMaster.eventGameCompleted += OnGameCompleted;
    }

    private new void OnDisable() {
        base.OnDisable();
        playerMaster.eventGameCompleted -= OnGameCompleted;
    }

    private void OnGameCompleted() {
        playerMaster.CallEventUpdateGameCompletedGUI();
        firstPersonController.MouseLook.SetCursorLock(false);
        firstPersonController.MouseLook.UpdateCursorLock();
        firstPersonController.enabled = false;
    }
}
