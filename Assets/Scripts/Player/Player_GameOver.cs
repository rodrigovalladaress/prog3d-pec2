﻿using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
/// <summary>
/// Juego terminado (porque el jugador cae a un precipicio o es derrotado).
/// </summary>
public class Player_GameOver : Player {

    private FirstPersonController firstPersonController;

    private new void Awake() {
        base.Awake();
        firstPersonController = GetComponent<FirstPersonController>();
    }

    private new void OnEnable() {
        base.OnEnable();
        playerMaster.eventDied += OnDied;
    }

    private new void OnDisable() {
        base.OnDisable();
        playerMaster.eventDied -= OnDied;
    }

    private void OnDied() {
        playerMaster.CallEventUpdateGameOverGUI();
        firstPersonController.MouseLook.SetCursorLock(false);
        firstPersonController.MouseLook.UpdateCursorLock();
        firstPersonController.enabled = false;
    }

}
