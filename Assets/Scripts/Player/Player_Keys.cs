﻿using System;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Gestión de la interacción del jugador con las llaves y las puertas.
/// </summary>
public class Player_Keys : Player {

    private Dictionary<Item_Key.KeyType, int> keys;     // Listado de llaves que puede llevar el 
                                                        // jugador y su cantidad.

    private new void Awake() {
        base.Awake();
        keys = new Dictionary<Item_Key.KeyType, int>();
        foreach (Item_Key.KeyType keyType in Enum.GetValues(typeof(Item_Key.KeyType))) {
            keys.Add(keyType, 0);
        }
    }

    private void Start() {
        playerMaster.CallEventUpdateKeysInfoGUI(keys);
    }

    private new void OnEnable() {
        base.OnEnable();
        playerMaster.eventKeyItemObtained += OnKeyItemObtained;
        playerMaster.eventTryToUnlockDoor += OnTryToUnlockDoor;
    }

    private new void OnDisable() {
        base.OnDisable();
        playerMaster.eventKeyItemObtained -= OnKeyItemObtained;
        playerMaster.eventTryToUnlockDoor -= OnTryToUnlockDoor;
    }

    private void OnKeyItemObtained(Item_Key.KeyType keyType) {
        if(keys.ContainsKey(keyType)) {
            keys[keyType]++;
            playerMaster.CallEventUpdateKeysInfoGUI(keys);
        } else {
            Debug.LogWarning(keyType + " doesn't exists in Player_Keys!");
        }
    }

    private void OnTryToUnlockDoor(Item_Key.KeyType keyType, Door_Master doorMaster) {
        if(keys.ContainsKey(keyType)) {
            if(keys[keyType] > 0) {
                keys[keyType]--;
                playerMaster.CallEventUpdateKeysInfoGUI(keys);
                doorMaster.CallEventUnlockDoor();
            }
        } else {
            Debug.LogWarning(keyType + " doesn't exists in Player_Keys!");
        }
    }
    
}
