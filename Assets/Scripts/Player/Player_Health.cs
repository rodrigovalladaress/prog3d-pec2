﻿using UnityEngine;
/// <summary>
/// Gestiona los cambios en la vida y el escudo del jugador.
/// </summary>
public class Player_Health : Player {

    public const int MAX_SHIELD = 100;
    public const int MAX_HEALTH = 100;
    
    [SerializeField]
    [Range(0, MAX_SHIELD)]
    private int initialShield = 100;
    [SerializeField]
    [Range(0, MAX_HEALTH)]
    private int initialHealth = 100;

    private int shield;
    private int health;
    private float shieldPercentage;         // Porcentaje de daño que evita el escudo actual.

    private new void Awake() {
        base.Awake();
    }

    private new void OnEnable() {
        base.OnEnable();
        playerMaster.eventDamaged += HarmPlayer;
        playerMaster.eventShieldItemObtained += OnShieldItemObtained;
        playerMaster.eventHealthItemObtained += OnHealthItemObtained;
    }

    private new void OnDisable() {
        base.OnDisable();
        playerMaster.eventDamaged -= HarmPlayer;
        playerMaster.eventShieldItemObtained -= OnShieldItemObtained;
        playerMaster.eventHealthItemObtained -= OnHealthItemObtained;
    }

    private void Start() {
        SetShield(initialShield);
        SetHealth(initialHealth);
    }

    private void HarmPlayer(int damage) {
        int healthDamage = (int)(damage * (1 - shieldPercentage));
        SetShield(shield - damage);
        SetHealth(health - healthDamage);
    }

    private void SetShield(int newShield) {
        if(newShield < 0) {
            newShield = 0;
        }
        if(shield != newShield) {
            shield = newShield;
            playerMaster.CallEventUpdateShieldGUI(newShield);
            shieldPercentage = shield / ((float)MAX_SHIELD);
        }
    }

    private void SetHealth(int newHealth) {
        if(newHealth < 0) {
            newHealth = 0;
        }
        if(health != newHealth) {
            health = newHealth;
            playerMaster.CallEventUpdateHealthGUI(newHealth);
            if(health == 0) {
                playerMaster.CallEventDied();
            }
        }
    }

    private void OnShieldItemObtained(int deltaShield) {
        shield += deltaShield;
        if(shield > MAX_SHIELD) {
            shield = MAX_SHIELD;
        }
        playerMaster.CallEventUpdateShieldGUI(shield);
    }

    private void OnHealthItemObtained(int deltaHealth) {
        health += deltaHealth;
        if (health > MAX_HEALTH) {
            health = MAX_SHIELD;
        }
        playerMaster.CallEventUpdateHealthGUI(health);
    }

}
