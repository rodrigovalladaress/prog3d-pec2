﻿using System.Collections;
using UnityEngine;
/// <summary>
/// Quejidos del jugador cuando es dañado.
/// </summary>
public class Player_Sound : Player {

    [SerializeField]
    private AudioClip[] damagedSounds;

    private AudioSource audioSource;
    private bool canPlaySound = true;

    private new void Awake() {
        base.Awake();
        audioSource = GetComponent<AudioSource>();
    }

    private new void OnEnable() {
        base.OnEnable();
        playerMaster.eventDamaged += OnDamaged;
    }

    private new void OnDisable() {
        base.OnDisable();
        playerMaster.eventDamaged -= OnDamaged;
    }

    private void OnDamaged(int damage) {
        if(canPlaySound) {
            int randomPos = Random.Range(0, damagedSounds.Length);
            AudioClip randomClip = damagedSounds[randomPos];
            audioSource.clip = randomClip;
            audioSource.Play();
            StartCoroutine(WaitForClipEnd(randomClip.length));
        }
    }

    private IEnumerator WaitForClipEnd(float seconds) {
        canPlaySound = false;
        yield return new WaitForSeconds(seconds);
        canPlaySound = true;
    }
    
}
