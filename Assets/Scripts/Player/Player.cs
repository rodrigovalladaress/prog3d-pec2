﻿using UnityEngine;
/// <summary>
/// Clase base para lo relacionado con el jugador. 
/// </summary>
public class Player : MonoBehaviour {

    private static int PARENT_LIMIT = 5;

    protected Player_Master playerMaster;

    protected void Awake() {
        Initialize();
    }

    protected void OnEnable() {
        playerMaster.eventDied += DisableThis;
    }

    protected void OnDisable() {
        playerMaster.eventDied -= DisableThis;
    }

    private void Initialize() {
        Transform from = transform;
        int checks = 0;
        do {
            playerMaster = from.GetComponent<Player_Master>();
            from = from.parent;
            checks++;
        } while (from && !playerMaster && checks < PARENT_LIMIT);
    }

    protected void DisableThis() {
        //enabled = false;
    }

}
