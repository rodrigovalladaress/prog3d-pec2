﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// Muestra la información del jugador por pantalla.
/// </summary>
public class Player_GUI : Player {

    [SerializeField]
    private Text shieldTextGUI;
    [SerializeField]
    private Text healthTextGUI;
    [SerializeField]
    private string shieldInfo = "Escudo: ";
    [SerializeField]
    private string healthInfo = "  Vida: ";
    [SerializeField]
    private Text weaponNameTextGUI;
    [SerializeField]
    private Text ammoTextGUI;
    [SerializeField]
    private string weaponAmmoInfo = "Munición: ";
    [SerializeField]
    private Text keysTextGUI;
    [SerializeField]
    private GameObject gameOverGUI;
    [SerializeField]
    private GameObject gameCompletedGUI;
    
    private new void OnEnable() {
        base.OnEnable();
        playerMaster.eventUpdateShieldGUI += OnUpdateShieldGUI;
        playerMaster.eventUpdateHealthGUI += OnUpdateHealthGUI;
        playerMaster.eventUpdateAmmoGUI += OnUpdateAmmoGUI;
        playerMaster.eventUpdateWeaponNameGUI += OnUpdateWeaponNameGUI;
        playerMaster.eventUpdateKeysInfoGUI += OnUpdateKeysInfoGUI;
        playerMaster.eventUpdateGameOverGUI += OnUpdateGameOverGUI;
        playerMaster.eventUpdateGameCompletedGUI += OnUpdateGameCompletedGUI;
    }

    private new void OnDisable() {
        base.OnDisable();
        playerMaster.eventUpdateShieldGUI -= OnUpdateShieldGUI;
        playerMaster.eventUpdateHealthGUI -= OnUpdateHealthGUI;
        playerMaster.eventUpdateAmmoGUI -= OnUpdateAmmoGUI;
        playerMaster.eventUpdateWeaponNameGUI -= OnUpdateWeaponNameGUI;
        playerMaster.eventUpdateKeysInfoGUI -= OnUpdateKeysInfoGUI;
        playerMaster.eventUpdateGameOverGUI -= OnUpdateGameOverGUI;
        playerMaster.eventUpdateGameCompletedGUI -= OnUpdateGameCompletedGUI;
    }

    private void OnUpdateShieldGUI(int shield) {
        shieldTextGUI.text = shieldInfo + shield;
    }

    private void OnUpdateHealthGUI(int health) {
        healthTextGUI.text = healthInfo + health;
    }

    private void OnUpdateAmmoGUI(int ammo, int maxAmmo) {
        ammoTextGUI.text = weaponAmmoInfo + ammo + "/" + maxAmmo;
    }

    private void OnUpdateWeaponNameGUI(string weaponName) {
        weaponNameTextGUI.text = weaponName;
    }

    private void OnUpdateKeysInfoGUI(Dictionary<Item_Key.KeyType, int> keys) {
        string info = "";
        foreach (KeyValuePair<Item_Key.KeyType, int> pair in keys) {
            if(pair.Value > 0) {
                info += Item_Key.GetKeyTypeName(pair.Key) + " x " + pair.Value + "\r\n";
            }
        }
        keysTextGUI.text = info;
    }

    private void OnUpdateGameOverGUI() {
        Debug.Log("Player GUI game over");
        gameOverGUI.SetActive(true);
    }

    private void OnUpdateGameCompletedGUI() {
        Debug.Log("Game completed");
        gameCompletedGUI.SetActive(true);
    }

}
