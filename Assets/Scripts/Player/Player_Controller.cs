﻿using UnityEngine;
/// <summary>
/// Recoge los inputs del jugador y llama a los eventos correspondientes.
/// </summary>
public class Player_Controller : Player {

    public const string FIRE_BUTTON = "Fire1";
    public const string WEAPON1_BUTTON = "Weapon1";
    public const string WEAPON2_BUTTON = "Weapon2";
    public const Weapon_Info.WeaponIdEnum WEAPON1_ID = Weapon_Info.WeaponIdEnum.Pistol;
    public const Weapon_Info.WeaponIdEnum WEAPON2_ID = Weapon_Info.WeaponIdEnum.MachineGun;

    private new void OnEnable() {
        base.OnDisable();
        playerMaster.eventDied += OnDied;
    }

    private new void OnDisable() {
        base.OnDisable();
        playerMaster.eventDied += OnDied;
    }

    void Update() {
        if (Input.GetButtonDown(FIRE_BUTTON) || Input.GetButton(FIRE_BUTTON)) {
            playerMaster.CallEventWeaponFire();
        }
        if (Input.GetButtonDown(WEAPON1_BUTTON)) {
            playerMaster.CallEventChangeWeapon(WEAPON1_ID);
        }
        if (Input.GetButtonDown(WEAPON2_BUTTON)) {
            playerMaster.CallEventChangeWeapon(WEAPON2_ID);
        }
    }

    private void OnDied() {
        enabled = false;
    }

}