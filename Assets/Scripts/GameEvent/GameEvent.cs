﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Gestión de los eventos de juego.
/// </summary>
public class GameEvent : MonoBehaviour {

    private static int PARENT_LIMIT = 5;

    protected GameEvent_Master gameEventMaster;

    protected void Awake() {
        Initialize();
    }

    protected void OnEnable() {
        gameEventMaster.eventCompleted += OnDisableThis;
    }

    protected void OnDisable() {
        gameEventMaster.eventCompleted -= OnDisableThis;
    }

    private void Initialize() {
        Transform from = transform;
        int checks = 0;
        do {
            gameEventMaster = from.GetComponent<GameEvent_Master>();
            from = from.parent;
            checks++;
        } while (from && !gameEventMaster && checks < PARENT_LIMIT);
    }

    protected void OnDisableThis() {
        Destroy(gameObject);
    }
}
