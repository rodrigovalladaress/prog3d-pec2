﻿using UnityEngine;
/// <summary>
/// Gestión de un evento de juego en el que hay que cambiar de estado una plataforma móvil como
/// resultado de completarlo.
/// </summary>
public class GameEvent_Result_ActivatePlatform : GameEvent {

    [SerializeField]
    private MovingPlatform_State platformState;
    [SerializeField]
    private MovingPlatform_State.PlatformStateEnum changeToState;

    private new void OnEnable() {
        base.OnEnable();
        gameEventMaster.eventCompleted += OnCompleted;
    }

    private new void OnDisable() {
        base.OnDisable();
        gameEventMaster.eventCompleted -= OnCompleted;
    }

    private new void Awake() {
        base.Awake();
        if(platformState.State == changeToState) {
            Debug.LogWarning(platformState.name + " is already unlocked!");
        }
    }

    private void OnCompleted() {
        platformState.State = changeToState; 
    }
}
