﻿using UnityEngine;
/// <summary>
/// Evento de juego en el que es necesario acabar con un/os enemigo/s.
/// </summary>
public class GameEvent_Mission_KillEnemies : GameEvent {

    [SerializeField]
    private Enemy_GameEventConnector[] enemies;

    private int numberOfEnemies;

    private new void Awake() {
        base.Awake();
        numberOfEnemies = enemies.Length;
        foreach (Enemy_GameEventConnector enemy in enemies) {
            enemy.GameEventMaster = gameEventMaster;
        }
    }

    private new void OnEnable() {
        base.OnEnable();
        gameEventMaster.eventEnemyDestroyed += OnEnemyDestroyed;
    }

    private new void OnDisable() {
        base.OnDisable();
        gameEventMaster.eventEnemyDestroyed -= OnEnemyDestroyed;
    }

    private void OnEnemyDestroyed() {
        numberOfEnemies--;
        if(numberOfEnemies <= 0) {
            gameEventMaster.CallEventCompleted();
        }
    }
    
}
