﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Animación de apertura y cierre de la puerta.
/// </summary>
public class Door_Animator : Door {

    [SerializeField]
    float secondsToClose = 4f;                          // Segundos que tarda la puerta en 
                                                        // cerrarse.

    private Door_Leaf[] doorLeaves;                     // Información sobre las posiciones de
                                                        // apertura y cierre de cada hoja de la 
                                                        // puerta.
    private Vector3[] doorLeavesStartedMovedPositions;  // Posiciones en las que comenzaron a
                                                        // moverse las hojas de las puertas cuando
                                                        // hubo un evento de apertura o cierre.
    private bool isOpening;                             // Indica si la puerta está abriéndose.
    private bool isClosing;                             // Indica si la puerta está cerrándose.
    private float accumulatedDeltaTime = 0f;            // Tiempo que ha transcurrido durante la
                                                        // interpolación actual.

    private new void Awake() {
        base.Awake();
        doorLeaves = GetComponentsInChildren<Door_Leaf>();
        doorLeavesStartedMovedPositions = new Vector3[doorLeaves.Length];
    }

    private new void OnEnable() {
        base.OnEnable();
        doorMaster.eventOpenDoor += OnDoorOpen;
        doorMaster.eventCloseDoor += OnDoorClose;
    }

    private new void OnDisable() {
        base.OnDisable();
        doorMaster.eventOpenDoor -= OnDoorOpen;
        doorMaster.eventCloseDoor -= OnDoorClose;
    }

    private void OnDoorOpen() {
        isOpening = true;
        isClosing = false;
        accumulatedDeltaTime = 0f;
        SetStartingPositionOfDoorLeaves();
    }

    private void OnDoorClose() {
        isClosing = true;
        isOpening = false;
        accumulatedDeltaTime = 0f;
        SetStartingPositionOfDoorLeaves();
    }

    void Update() {
        if (isOpening || isClosing) {
            for (int i = 0; i < doorLeaves.Length; i++) {
                Door_Leaf doorLeaf = doorLeaves[i];
                Vector3 startedMovedPos = doorLeavesStartedMovedPositions[i];
                Vector3 startPoint;
                Vector3 endPoint;
                float distanceAlreadyMoved;
                float percentage;
                float percentageAlreadyCompleted;
                float secondsAlreadyPassed; // Tiempo que habría tenido que transcurrir para que la
                // hoja de la puerta se moviera a la posición en la que empezó a moverse
                // (startedMovedPos) si hubiera empezado a moverse desde la posición inicial progra-
                // mada (startPoint).
                accumulatedDeltaTime += Time.deltaTime;
                if (isOpening) {
                    startPoint = doorLeaf.ClosedPosition.transform.position;
                    endPoint = doorLeaf.OpenedPosition.transform.position;
                }
                else {
                    startPoint = doorLeaf.OpenedPosition.transform.position;
                    endPoint = doorLeaf.ClosedPosition.transform.position;
                }
                distanceAlreadyMoved = (startedMovedPos - startPoint).magnitude;
                percentageAlreadyCompleted = distanceAlreadyMoved / doorLeaf.TotalDistance;
                secondsAlreadyPassed = percentageAlreadyCompleted * secondsToClose;
                percentage = (accumulatedDeltaTime + secondsAlreadyPassed) / secondsToClose;
                if (percentage >= 1f) {
                    isOpening = false;
                    isClosing = false;
                    doorMaster.CallEventFinishedMovingDoor();
                }
                else {
                    doorLeaf.transform.position = Vector3.Slerp(startPoint,
                        endPoint, percentage);
                }
            }
        }
    }

    private void SetStartingPositionOfDoorLeaves() {
        for (int i = 0; i < doorLeaves.Length; i++) {
            doorLeavesStartedMovedPositions[i] = doorLeaves[i].transform.position;
        }
    }

}
