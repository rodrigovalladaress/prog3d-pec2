﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Trigger de una puerta. Cuando el jugador entra dentro, esta se abre (si no necesita llave o el
/// jugador tiene la llave necesaria.
/// </summary>
public class Door_Trigger : Door {

    private bool isLocked = false;

    private new void OnEnable() {
        base.OnEnable();
        doorMaster.eventDoorIsLocked += OnDoorSetLock;
    }

    private new void OnDisable() {
        base.OnDisable();
        doorMaster.eventDoorIsLocked -= OnDoorSetLock;
    }

    private void OnDoorSetLock(bool isLocked) {
        this.isLocked = isLocked;
        // Si la puerta se desbloquea, esta se abre automáticamente para que el jugador no tenga
        // que entrar y salir del trigger.
        if (!isLocked) {
            doorMaster.CallEventOpenDoor();
        }
    }

    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag(References.Player.tag)) {
            if(!isLocked) {
                doorMaster.CallEventOpenDoor();
            } else {
                doorMaster.CallEventTryToOpenLockedDoor();
            }
        }
    }

    private void OnTriggerExit(Collider other) {
        if (other.CompareTag(References.Player.tag)) {
            doorMaster.CallEventDoorClose();
        }
    }

}
