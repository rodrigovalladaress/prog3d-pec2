﻿using UnityEngine;
/// <summary>
/// Guarda la información de la hoja de una puerta.
/// </summary>
[System.Serializable]
public class Door_Leaf : MonoBehaviour {

    [SerializeField]
    private Transform openedPosition;   // Posición de apertura.
    [SerializeField]
    private Transform closedPosition;   // Posición de cierre.
    [SerializeField]
    private bool isOpened;              // Indica si la puerta ya está abierta.

    private float totalDistance;        // Distancia entre las posiciones de apertura y cierre de
                                        // la puerta.

    private void Awake() {
        totalDistance = (closedPosition.position - openedPosition.position).magnitude;
    }

    public Transform OpenedPosition {
        get {
            return openedPosition;
        }
    }

    public Transform ClosedPosition {
        get {
            return closedPosition;
        }
    }

    public bool IsOpened {
        get {
            return isOpened;
        }
    }

    public float TotalDistance {
        get {
            return totalDistance;
        }
    }
}
