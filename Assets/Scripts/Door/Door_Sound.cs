﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Sonido de la puerta cuando se abre y cierra.
/// </summary>
public class Door_Sound : Door {

    private AudioSource audioSource;

    private new void Awake() {
        base.Awake();
        audioSource = GetComponent<AudioSource>();
    }

    private new void OnEnable() {
        base.OnEnable();
        doorMaster.eventCloseDoor += OnDoorClosing;
        doorMaster.eventOpenDoor += OnDoorOpening;
        doorMaster.eventFinishedMovingDoor += OnFinishedMovingDoor;
    }

    private new void OnDisable() {
        base.OnDisable();
        doorMaster.eventCloseDoor -= OnDoorClosing;
        doorMaster.eventOpenDoor -= OnDoorOpening;
        doorMaster.eventFinishedMovingDoor += OnFinishedMovingDoor;
    }

    private void OnDoorOpening() {
        PlaySound();
    }

    private void OnDoorClosing() {
        PlaySound();
    }

    private void PlaySound() {
        audioSource.loop = true;
        audioSource.Play();
    }

    private void OnFinishedMovingDoor() {
        audioSource.Stop();
    }
    
}
