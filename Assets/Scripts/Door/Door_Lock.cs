﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Simula que la puerta está cerrada con una llave del tipo keyType.
/// </summary>
public class Door_Lock : Door {

    [SerializeField]
    private bool isLocked = false;
    [SerializeField]
    private Item_Key.KeyType keyType;


    private new void OnEnable() {
        base.OnEnable();
        doorMaster.eventUnlockDoor += OnDoorUnlocked;
        doorMaster.eventTryToOpenLockedDoor += OnTryToOpenLockedDoor;
    }

    private new void OnDisable() {
        base.OnDisable();
        doorMaster.eventUnlockDoor -= OnDoorUnlocked;
        doorMaster.eventTryToOpenLockedDoor -= OnTryToOpenLockedDoor;
    }

    private void Start() {
        doorMaster.CallEventDoorIsLocked(isLocked);
    }

    private void OnTryToOpenLockedDoor() {
        doorMaster.CallEventCheckIfPlayerCanUnlockDoor(keyType);
    }

    private void OnDoorUnlocked() {
        isLocked = false;
        doorMaster.CallEventDoorIsLocked(isLocked);
    }

}
