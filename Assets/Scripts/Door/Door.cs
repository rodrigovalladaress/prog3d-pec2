﻿using UnityEngine;
/// <summary>
/// Clase base para los scripts relacionados con una puerta.
/// </summary>
public class Door : MonoBehaviour {

    private static int PARENT_LIMIT = 5;

    protected Door_Master doorMaster;

    protected void Awake() {
        Initialize();
    }

    protected void OnEnable() {
        doorMaster.eventDestroy += DisableThis;
    }

    protected void OnDisable() {
        doorMaster.eventDestroy -= DisableThis;
    }

    private void Initialize() {
        Transform from = transform;
        int checks = 0;
        do {
            doorMaster = from.GetComponent<Door_Master>();
            from = from.parent;
            checks++;
        } while (from && !doorMaster && checks < PARENT_LIMIT);
    }

    protected void DisableThis() {
        enabled = false;
    }

}
