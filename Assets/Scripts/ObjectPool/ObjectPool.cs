﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Genera una lista de objetos que se instancian al principio del nivel, para no tener que instan-
/// ciarlos en tiempo de ejecución.
/// </summary>
public class ObjectPool : MonoBehaviour {

    public const int NOT_FOUND = -1;

    [SerializeField]
    private ObjectPoolInitializer[] objectPoolInitializer;     // Inicializador de ObjectPools 
                                                               // desde el editor.

    private List<List<GameObject>> pool;                       // Lista de GameObjects instanciados
    private string[] names;                                    // Nombres de los GameObjects
                                                               // instanciados.
    private int[] count;                                       // Número de GameObjects instan
                                                               // ciados de cada tipo.
    private int[] iterators;                                   // Iterador circular de la lista 
                                                               // pool. Cuando un GameObject es 
                                                               // solicitado, aumentará su iterador
                                                               // en 1.

    private void Awake () {
        InitializePool();
	}

    private void InitializePool() {
        int objectCount = objectPoolInitializer.Length;
        GameObject aux = new GameObject(); // GameObject auxiliar para crear los parents de cada
                                           // tipo de GameObject instanciado.
        Destroy(aux);
        pool = new List<List<GameObject>>();
        names = new string[objectCount];
        count = new int[objectCount];
        iterators = new int[objectCount];
        for (int i = 0; i < objectCount; i++) {
            List<GameObject> currentPool;
            ObjectPoolInitializer currentPoolInitializer = objectPoolInitializer[i];
            GameObject currentPrefab = currentPoolInitializer.Prefab;
            string currentName = currentPrefab.name;
            int currentCount = currentPoolInitializer.Count;
            Transform currentRoot = Instantiate(aux, transform).transform;
            currentRoot.name = currentName;
            names[i] = currentName;
            count[i] = currentCount;
            iterators[i] = 0;
            pool.Add(new List<GameObject>());
            currentPool = pool[i];
            for (int j = 0; j < currentCount; j++) {
                GameObject o = Instantiate(currentPrefab, Vector3.zero,
                    Quaternion.identity, currentRoot);
                ObjectPoolElement e = o.GetComponent<ObjectPoolElement>();
                o.gameObject.SetActive(false);
                o.name = currentName + "_" + (j + 1);
                if (e) {
                    e.ObjectPoolRoot = currentRoot;
                }
                currentPool.Add(o);
            }
        }
    }

    public GameObject Request(int i) {
        return Request(i, Vector3.zero, Quaternion.identity, null);
    }

    public GameObject Request(int i, Vector3 position, Quaternion rotation) {
        return Request(i, position, rotation, null);
    }

    public GameObject Request(int i, Vector3 position, Quaternion rotation, Transform parent) {
        int j = iterators[i];
        GameObject r = pool[i][j].gameObject;
        ObjectPoolElement e = r.GetComponent<ObjectPoolElement>();
        // Reiniciar el estado del gameobject si ha sido instanciado
        // anteriormente.
        if (r.activeInHierarchy) {
            if(e) {
                e.Deactivate();
            } else {
                r.SetActive(false);
            }
        }
        r.SetActive(true);
        r.transform.position = position;
        r.transform.rotation = rotation;
        r.transform.parent = parent;
        iterators[i] = (iterators[i] + 1) % count[i];
        return r;
    }

    public int GetPoolPosition(string nameParam) {
        int r = NOT_FOUND;
        int i = 0;
        while (i < names.Length && names[i] != nameParam) {
            i++;
        }
        if(i != names.Length) {
            r = i;
        } else {
            Debug.LogWarning(nameParam + " has not been found!");
        }
        return r;
    }
}
