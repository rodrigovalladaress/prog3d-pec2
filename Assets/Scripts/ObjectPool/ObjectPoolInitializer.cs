﻿using UnityEngine;
/// <summary>
/// Permite inicializar el ObjectPool desde el editor.
/// </summary>
[System.Serializable]
public class ObjectPoolInitializer {

    [SerializeField]
    private GameObject prefab;
    [SerializeField]
    private int count;

    public GameObject Prefab {
        get {
            return prefab;
        }
    }

    public int Count {
        get {
            return count;
        }
    }
}