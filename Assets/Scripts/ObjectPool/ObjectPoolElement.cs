﻿using UnityEngine;
/// <summary>
/// Cuando un objeto del ObjectPool es desactivado, cambia su parent para que esté dentro de la 
/// jeraraquía de ObjectPools (útil para debuggear el uso de los ObjectPools en cada momento).
/// </summary>
public class ObjectPoolElement : MonoBehaviour {

    private Transform _objectPoolRoot;  // Parent de este tipo de ObjectPool. Se inicializa en 
                                        // ObjectPool.

    public Transform ObjectPoolRoot {
        set {
            _objectPoolRoot = value;
        }
    }

    public void Deactivate() {
        transform.parent = _objectPoolRoot;
        gameObject.SetActive(false);
    }

}
