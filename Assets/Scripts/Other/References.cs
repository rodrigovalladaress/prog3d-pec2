﻿using UnityEngine;
/// <summary>
/// Referencias a objetos de la escena.
/// </summary>
public class References : MonoBehaviour {

    [SerializeField]
    private Transform player;
    [SerializeField]
    private ObjectPool objectPool;
    [SerializeField]
    private Player_Master playerMaster;
    [SerializeField]
    private Weapon_Master weaponMaster;

    private static Weapon_Info[] s_weaponList;     // Lista de armas que puede llevar el jugador. Estas son 
                                                   // hijos del GameObject Weapon con un componente Weapon_Id.

    private static Transform s_player;
    private static ObjectPool s_objectPool;
    private static Player_Master s_playerMaster;
    private static Weapon_Master s_weaponMaster;
    private static string s_playerTag;

    private void Awake() {
        s_player = player;
        s_objectPool = objectPool;
        s_playerMaster = playerMaster;
        s_weaponMaster = weaponMaster;
        s_weaponList = s_weaponMaster.GetComponentsInChildren<Weapon_Info>(true);
        s_playerTag = s_player.tag;
    }

    public static Transform Player {
        get {
            return s_player;
        }
    }

    public static ObjectPool ObjectPool {
        get {
            return s_objectPool;
        }
    }

    public static Player_Master PlayerMaster {
        get {
            return s_playerMaster;
        }
    }

    public static string PlayerTag {
        get {
            return s_playerTag;
        }
    }

    public static Weapon_Master WeaponMaster {
        get {
            return s_weaponMaster;
        }
    }

    public static Weapon_Info[] WeaponList {
        get {
            return s_weaponList;
        }
    }

    
}
