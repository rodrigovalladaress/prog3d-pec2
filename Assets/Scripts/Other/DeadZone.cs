﻿using UnityEngine;

public class DeadZone : MonoBehaviour {

    private void OnTriggerEnter(Collider other) {
        if(References.Player.CompareTag(other.tag)) {
            References.PlayerMaster.CallEventDied();
        }
    }
    
}
