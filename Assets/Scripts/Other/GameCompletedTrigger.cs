﻿using UnityEngine;

public class GameCompletedTrigger : MonoBehaviour {

    private void OnTriggerEnter(Collider other) {
        if(References.Player.CompareTag(other.tag)) {
            References.PlayerMaster.CallEventGameCompleted();
        }
    }

}
