﻿using UnityEngine;

public class Explosion : MonoBehaviour {

    private AudioSource audioSource;
    private ParticleSystem particle;

	private void Awake () {
        audioSource = GetComponent<AudioSource>();
        particle = GetComponentInChildren<ParticleSystem>();
    }

    private void OnEnable() {
        audioSource.Play();
        particle.Play();
    }

}
