﻿using UnityEngine;

public static class PECUtilities {

    public static bool Near(Vector3 a, Vector3 b, float epsilon) {
        return Vector3.Distance(a, b) <= epsilon;
    }

}
