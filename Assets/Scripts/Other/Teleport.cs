﻿using UnityEngine;
/// <summary>
/// Teletransporte a otra escena.
/// </summary>
public class Teleport : MonoBehaviour {

    [SerializeField]
    private string scene;

    private void OnTriggerEnter(Collider other) {
        LevelManager.SLoadScene(scene);
    }
    
}