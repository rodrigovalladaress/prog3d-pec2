﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {

    [SerializeField]
    private string firstScene;

    private static string s_FirstScene;

    private void Awake() {
        s_FirstScene = firstScene;
    }

    public void ReloadScene() {
        SReloadScene();
    }

    public static void SReloadScene() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void LoadScene(string scene) {
        SLoadScene(scene);
    }

    public static void SLoadScene(string scene) {
        SceneManager.LoadScene(scene);
    }

    public void LoadFirstScene() {
        SLoadScene(s_FirstScene);
    }

    public void Exit() {
        SExit();
    }
    
    public static void SExit() {
        Application.Quit();
    }

}
