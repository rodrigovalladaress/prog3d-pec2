﻿using UnityEngine;
/// <summary>
/// Clase genérica de una Máquina de Estados Finita.
/// </summary>
public abstract class FSM {

    protected StateList genericStates;
    protected State genericCurrentState;
    protected Info genericFsmInfo;
    private Enemy_Master enemyMaster;

    public FSM(Enemy_Master enemyMaster) {
        this.enemyMaster = enemyMaster;
    }

    /*~FSM() {
        enemyMaster.eventEnemyFSMState -= OnCurrentStateChanged;
    }*/

    public void UpdateState () {
        if(genericCurrentState != null) {
            State nextState = genericCurrentState.NextState(genericFsmInfo);
            if(nextState != genericCurrentState) {
                Debug.Log("From " + genericCurrentState.ToString() + " to " 
                    + nextState.ToString());
                enemyMaster.CallEventFSMStateChanged(nextState, genericStates);
            }
            genericCurrentState = nextState;
        }
	}

    public abstract class State {

        protected StateList states;

        public State(StateList states) {
            this.states = states;
        }

        public abstract State NextState(Info info);
    }

    public abstract class StateList {
        // Lista de estados de una FSM.
    }

    public abstract class Info {
        // Información que se pasa a una FSM para que esta calcule el siguiente estado.
    }

}
