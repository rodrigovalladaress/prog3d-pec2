﻿using UnityEngine;
/// <summary>
/// Máquina de estados finita de un dron.
/// </summary>
public class DroneFSM : FSM {

    public DroneFSM(Enemy_Master enemyMaster) : base(enemyMaster) {
        States = new DroneStateList();
        States.Patrol = new PatrolState(States);
        States.Alert = new AlertState(States);
        States.Attack = new AttackState(States);
        FsmInfo = new DroneInfo();
        CurrentState = States.Patrol;
    }

    public DroneState CurrentState {
        get {
            return genericCurrentState as DroneState;
        }

        private set {
            genericCurrentState = value;
        }
    }

    public DroneInfo FsmInfo {
        get {
            return genericFsmInfo as DroneInfo;
        }

        private set {
            genericFsmInfo = value;
        }
    }

    public DroneStateList States {
        get {
            return genericStates as DroneStateList;
        }

        private set {
            genericStates = value;
        }
    }

    public class DroneStateList : StateList {
        private PatrolState patrol;
        private AlertState alert;
        private AttackState attack;

        #region Properties
        public PatrolState Patrol {
            get {
                return patrol;
            }

            set {
                patrol = value;
            }
        }

        public AlertState Alert {
            get {
                return alert;
            }

            set {
                alert = value;
            }
        }

        public AttackState Attack {
            get {
                return attack;
            }

            set {
                attack = value;
            }
        }
        #endregion
    }

    public abstract class DroneState : State {

        public DroneState(StateList stateList) : base(stateList) { }

        protected DroneStateList States {
            get {
                return states as DroneStateList;
            }
        }

    }

    /// <summary>
    /// Estado de patrulla (estado por defecto).
    /// Cuando es disparado por un jugador, va al estado Attack.
    /// Cuando el jugador entra en su rango de acción, va a Alert.
    /// </summary>
    public class PatrolState : DroneState {

        public PatrolState(StateList stateList) : base(stateList) { }

        public override State NextState(Info info) {
            State nextState = this;
            DroneInfo droneInfo = info as DroneInfo;
            if (droneInfo.shootedByPlayer) {
                nextState = States.Alert;
            }
            else if (droneInfo.playerInActionRange) {
                nextState = States.Alert;
            } else if(droneInfo.playerInView) {
                nextState = States.Attack;
            }
            return nextState;
        }

    }

    /// <summary>
    /// Estado de alerta.
    /// Cuando es disparado por el jugador, va al estado Attack.
    /// Cuando el jugador entra en su rango de visión, va a Attack.
    /// Cuando este enemigo completa una rotación, va al estado Patrol.
    /// </summary>
    public class AlertState : DroneState {

        public AlertState(StateList stateList) : base(stateList) { }

        public override State NextState(Info info) {
            State nextState = this;
            DroneInfo droneInfo = info as DroneInfo;
            /*Debug.Log("AlertState: shooted by player = " + droneInfo.shootedByPlayer);
            Debug.Log("AlertState: player in view = " + droneInfo.playerInView);
            Debug.Log("AlertState: rotating = " + droneInfo.rotating);*/
            if (droneInfo.playerInView) {
                nextState = States.Attack;
            }
            else {
                if (!droneInfo.rotating) {
                    nextState = States.Patrol;
                }
            }
            return nextState;
        }

    }

    /// <summary>
    /// Estado de ataque.
    /// Cuando el jugador deja de estar en el rango de visión de este enemigo, va al estado Alert.
    /// </summary>
    public class AttackState : DroneState {

        public AttackState(StateList stateList) : base(stateList) { }

        public override State NextState(Info info) {
            State nextState = this;
            DroneInfo droneInfo = info as DroneInfo;
            //Debug.Log("AttackState: in view = " + droneInfo.playerInView);
            if (!droneInfo.playerInView) {
                nextState = States.Alert;
            }
            return nextState;
        }

    }

    public class DroneInfo : Info {
        public bool shootedByPlayer = false;
        public bool playerInActionRange = false;
        public bool rotating = false;
        public bool playerInView = false;
    }

}
