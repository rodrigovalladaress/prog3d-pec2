﻿using UnityEngine;
/// <summary>
/// Ítem de escudo del jugador.
/// </summary>
public class Item_Shield : Item {

    [SerializeField]
    [Range(0, Player_Health.MAX_SHIELD)]
    private int shield;

    private new void OnEnable() {
        base.OnEnable();
        itemMaster.eventItemObtained += OnItemObtained;
    }

    private new void OnDisable() {
        base.OnDisable();
        itemMaster.eventItemObtained -= OnItemObtained;
    }

    private void OnItemObtained() {
        itemMaster.CallEventShieldItemObtained(shield);
    }
}
