﻿using UnityEngine;
/// <summary>
/// Recogida del ítem cuando el jugador entra en el trigger.
/// </summary>
public class Item_Trigger : Item {

    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag(References.Player.tag)) {
            itemMaster.CallEventItemObtained();
        }
    }

}
