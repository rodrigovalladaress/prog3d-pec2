﻿using System.Collections;
using UnityEngine;
/// <summary>
/// Clase base de un ítem.
/// </summary>
public class Item : MonoBehaviour {

    private static int PARENT_LIMIT = 5;

    protected Item_Master itemMaster;
    private ObjectPoolElement objectPoolElement;
    private float secondsToDisable = 0f;

    protected void Awake() {
        AudioSource audioSource = GetComponent<AudioSource>();
        Initialize();
        if(audioSource) {
            secondsToDisable = audioSource.clip.length;
        }
    }

    protected void OnEnable() {
        itemMaster.eventItemObtained += DisableThis;
    }

    protected void OnDisable() {
        itemMaster.eventItemObtained -= DisableThis;
    }

    private void Initialize() {
        Transform from = transform;
        int checks = 0;
        do {
            itemMaster = from.GetComponent<Item_Master>();
            from = from.parent;
            checks++;
        } while (from && !itemMaster && checks < PARENT_LIMIT);
        objectPoolElement = GetComponent<ObjectPoolElement>();
    }

    // Dependiendo de si el ítem se instanció desde el ObjectPool, realizar distintas acciones.
    protected void DisableThis() {
        if (objectPoolElement != null) {
            StartCoroutine(DeactivateInSeconds(secondsToDisable));
        }
        else {
            StartCoroutine(DestroyInSeconds(secondsToDisable));
        }
    }

    private IEnumerator DeactivateInSeconds(float seconds) {
        yield return new WaitForSeconds(seconds);
        objectPoolElement.Deactivate();
    }

    private IEnumerator DestroyInSeconds(float seconds) {
        yield return new WaitForSeconds(seconds);
        Destroy(gameObject);
    }

}
