﻿using UnityEngine;
/// <summary>
/// Sonido de obtención del ítem.
/// </summary>
public class Item_Sound : Item {

    private AudioSource audioSource;

    private new void Awake() {
        base.Awake();
        audioSource = GetComponent<AudioSource>();
    }

    protected new void OnEnable() {
        itemMaster.eventItemObtained += OnEventObtained;
    }

    protected new void OnDisable() {
        itemMaster.eventItemObtained -= OnEventObtained;
    }

    private void OnEventObtained() {
        audioSource.Play();
    }
    
}
