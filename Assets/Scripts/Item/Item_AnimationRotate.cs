﻿using UnityEngine;
/// <summary>
/// Animación de rotación de un ítem.
/// </summary>
public class Item_AnimationRotate : Item {

    private float ANGLES_MULTIPLIER = 20f;

    [SerializeField]
    private float speed = 5f;

	void Update () {
        transform.Rotate(0, ANGLES_MULTIPLIER * speed * Time.deltaTime, 0);
	}
}
