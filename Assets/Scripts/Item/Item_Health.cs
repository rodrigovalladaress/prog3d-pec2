﻿using UnityEngine;
/// <summary>
/// Ítem de vida del jugador.
/// </summary>
public class Item_Health : Item {

    [SerializeField]
    [Range(0, Player_Health.MAX_HEALTH)]
    private int health;

    private new void OnEnable() {
        base.OnEnable();
        itemMaster.eventItemObtained += OnItemObtained;
    }

    private new void OnDisable() {
        base.OnDisable();
        itemMaster.eventItemObtained -= OnItemObtained;
    }

    private void OnItemObtained() {
        itemMaster.CallEventHealthItemObtained(health);
    }

}
