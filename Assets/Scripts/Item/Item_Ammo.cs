﻿using UnityEngine;
/// <summary>
/// Munición de un tipo de arma indicado.
/// </summary>
public class Item_Ammo : Item {

    [SerializeField]
    private Weapon_Info.WeaponIdEnum weaponId;      // Tipo de arma.

    [SerializeField]
    [Range(0, Weapon_Info.MAX_AMMO)]
    private int ammo;                               // Munición que se obtiene.

    private new void OnEnable() {
        base.OnEnable();
        itemMaster.eventItemObtained += OnItemObtained;
    }

    private new void OnDisable() {
        base.OnDisable();
        itemMaster.eventItemObtained -= OnItemObtained;
    }

    private void OnItemObtained() {
        itemMaster.CallEventAmmoItemObtained(weaponId, ammo);
    }

}
