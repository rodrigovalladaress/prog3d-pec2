﻿using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Ítem de llave.
/// </summary>
public class Item_Key : Item {

    public enum KeyType {                               // Tipos de llave implementados.
        BasicKey
    }

    private static Dictionary<KeyType, string> keyNames // Nombre de cada tipo de llave.
        = new Dictionary<KeyType, string>() {
        { KeyType.BasicKey, "Llave básica" }
    };

    [SerializeField]
    private KeyType keyType;

    private new void OnEnable() {
        base.OnEnable();
        itemMaster.eventItemObtained += OnItemObtained;
    }

    private new void OnDisable() {
        base.OnDisable();
        itemMaster.eventItemObtained -= OnItemObtained;
    }

    private void OnItemObtained() {
        itemMaster.CallEventKeyItemObtained(keyType);
    }

    public static string GetKeyTypeName(KeyType s_keyType) {
        string r = "ERROR";
        if(keyNames.ContainsKey(s_keyType)) {
            r = keyNames[s_keyType];
        }
        return r;
    }
}
