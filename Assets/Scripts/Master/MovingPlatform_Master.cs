﻿using UnityEngine;
/// <summary>
/// Gestión de los eventos relacionados con una plataforma móvil.
/// </summary>
public class MovingPlatform_Master : MonoBehaviour {

    public delegate void GeneralEventHandler();
    public event GeneralEventHandler eventDestroy;
    public event GeneralEventHandler eventPositionReached;

    public delegate void PlayerOnPlatformEventHandler(bool isPlayerOnPlatform);
    public event PlayerOnPlatformEventHandler eventPlayerOnPlatform;

    public delegate void PlatformStateEventHandler(MovingPlatform_State.PlatformStateEnum platformState);
    public event PlatformStateEventHandler eventPlatformStateChanged;

    public void CallEventPositionReached() {
        if(eventPositionReached != null) {
            eventPositionReached();
        }
    }

    public void CallEventPlayerOnPlatform(bool isPlayerOnPlatform) {
        if(eventPlayerOnPlatform != null) {
            eventPlayerOnPlatform(isPlayerOnPlatform);
        }
    }

    public void CallEventPlatformStateChanged
        (MovingPlatform_State.PlatformStateEnum platformState) {
        if(eventPlatformStateChanged != null) {
            eventPlatformStateChanged(platformState);
        }
    }

}
