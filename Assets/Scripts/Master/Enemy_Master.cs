﻿using UnityEngine;
/// <summary>
/// Gestión de los eventos que puede ocasionar un enemigo.
/// </summary>
public class Enemy_Master : MonoBehaviour {

    public delegate void GeneralEventHandler();
    public event GeneralEventHandler eventDie;
    public event GeneralEventHandler eventPatrol;
    public event GeneralEventHandler eventAttack;
    public event GeneralEventHandler eventTargetReached;
    public event GeneralEventHandler eventPlayerIsNear;
    public event GeneralEventHandler eventPlayerIsFar;
    public event GeneralEventHandler eventPlayerIsInView;
    public event GeneralEventHandler eventPlayerIsNotLongerInView;
    public event GeneralEventHandler eventShootedByPlayer;
    public event GeneralEventHandler eventShooted;
    public event GeneralEventHandler eventStartRotating;
    public event GeneralEventHandler eventStopRotating;

    public delegate void HealthEventHandler(int damage);
    public event HealthEventHandler eventDamaged;
    public event HealthEventHandler eventDamagePlayer;

    public delegate void NavTargetEventHandler(Transform target);
    public event NavTargetEventHandler eventSetPatrolTarget;

    public delegate void FSMStateEventHandler(FSM.State state, FSM.StateList states);
    public event FSMStateEventHandler eventFSMStateChanged;

    // GeneralEventHandler

    public void CallEventDie() {
        if(eventDie != null) {
            eventDie();
        }
    }

	public void CallEventPatrol() {
        if (eventPatrol != null) {
            eventPatrol();
        }
    }

    public void CallEventAttack() {
        if (eventAttack != null) {
            eventAttack();
        }
    }

    public void CallEventTargetReached() {
        if (eventTargetReached != null) {
            eventTargetReached();
        }
    }

    public void CallEventPlayerIsNear() {
        if (eventPlayerIsNear != null) {
            eventPlayerIsNear();
        }
    }

    public void CallEventPlayerIsFar() {
        if (eventPlayerIsFar != null) {
            eventPlayerIsFar();
        }
    }

    public void CallEventPlayerIsInView() {
        if (eventPlayerIsInView != null) {
            eventPlayerIsInView();
        }
    }

    public void CallEventPlayerIsNotLongInView() {
        if (eventPlayerIsNotLongerInView != null) {
            eventPlayerIsNotLongerInView();
        }
    }

    public void CallEventShootedByPlayer() {
        if (eventShootedByPlayer != null) {
            eventShootedByPlayer();
        }
    }

    public void CallEventShooted() {
        if (eventShooted != null) {
            eventShooted();
        }
    }

    public void CallEventStartedRotation() {
        if (eventStartRotating != null) {
            eventStartRotating();
        }
    }

    public void CallEventStoppedRotation() {
        if (eventStopRotating != null) {
            eventStopRotating();
        }
    }

    // HealthEventHandler

    public void CallEventEnemyDamaged(int damage) {
        if(eventDamaged != null) {
            eventDamaged(damage);
        }
    }

    public void CallEventDamagePlayer(int damage) {
        if(eventDamagePlayer != null) {
            eventDamagePlayer(damage);
        }
        References.PlayerMaster.CallEventDamaged(damage);
    }

    // NavTargetEventHandler

    public void CallEventSetPatrolTarget(Transform target) {
        if (eventSetPatrolTarget != null) {
            eventSetPatrolTarget(target);
        }
    }

    // FSMStateEventHandler

    public void CallEventFSMStateChanged(FSM.State state, FSM.StateList states) {
        if(eventFSMStateChanged != null) {
            eventFSMStateChanged(state, states);
        }
    }

}
