﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Eventos relacioandos con las armas del jugador.
/// </summary>
public class Weapon_Master : MonoBehaviour {

    public delegate void GeneralEventHandler();
    public event GeneralEventHandler eventRequestWeaponFire;
    public event GeneralEventHandler eventSuccesfullyFiredWeapon;

    public delegate void ChangeWeaponEventHandler(Weapon_Info.WeaponIdEnum weapon);
    public event ChangeWeaponEventHandler eventChangeWeapon;

    public delegate void AmmoEventHandler(int ammo);
    public event AmmoEventHandler eventAmmoChanged;
    public event AmmoEventHandler eventReduceAmmoBy;

    public delegate void SuccesfullyChangedWeaponEventHandler(Weapon_Info weaponInfo);
    public event SuccesfullyChangedWeaponEventHandler eventSuccesfullyChangedWeapon;

    public delegate void AmmoItemObtainedEventHandler(Weapon_Info.WeaponIdEnum weaponId, int deltaAmmo);
    public event AmmoItemObtainedEventHandler eventAmmoItemObtained;

    public delegate void EnoughAmmoForFiringEventHandler(bool enoughAmmo);
    public event EnoughAmmoForFiringEventHandler eventEnoughAmmoForFiring;

    public delegate void UpdateAmmoGUIEventHandler(int ammo, int maxAmmo);
    public event UpdateAmmoGUIEventHandler eventUpdateAmmoGUI;

    public delegate void UpdateWeaponNameGUIEventHandler(string weaponName);
    public event UpdateWeaponNameGUIEventHandler eventUpdateWeaponNameGUI;

    // GeneralEventHandler

    public void CallEventRequestWeaponFire() {
        if (eventRequestWeaponFire != null) {
            eventRequestWeaponFire();
        }
    }

    public void CallEventSuccesfullyFiredWeapon() {
        if (eventSuccesfullyFiredWeapon != null) {
            eventSuccesfullyFiredWeapon();
        }
    }

    // ChangeWeaponEventHandler

    public void CallEventChangeWeapon(Weapon_Info.WeaponIdEnum weapon) {
        if (eventChangeWeapon != null) {
            eventChangeWeapon(weapon);
        }
    }

    // AmmoEventHandler
    public void CallEventAmmoChanged(int ammo) {
        if (eventAmmoChanged != null) {
            eventAmmoChanged(ammo);
        }
        References.PlayerMaster.CallEventAmmoChanged(ammo);
    }

    public void CallEventReduceAmmoBy(int deltaAmmo) {
        if (eventAmmoChanged != null) {
            eventReduceAmmoBy(deltaAmmo);
        }
    }    

    // SuccesfullyChangedWeaponEventHandler

    public void CallEventSuccesfullyChangedWeapon(Weapon_Info weaponInfo) {
        if (eventSuccesfullyChangedWeapon != null) {
            eventSuccesfullyChangedWeapon(weaponInfo);
        }
        References.PlayerMaster.CallEventSuccesfullyChangedWeapon(weaponInfo);
    }

    // AmmoItemObtainedEventHandler

    public void CallEventAmmoItemObtained(Weapon_Info.WeaponIdEnum weaponId, int deltaAmmo) {
        if(eventAmmoItemObtained != null) {
            eventAmmoItemObtained(weaponId, deltaAmmo);
        }
    }

    // EnoughAmmoForFiringEventHandler

    public void CallEventEnoughAmmoForFiring(bool enoughAmmo) {
        if(eventEnoughAmmoForFiring != null) {
            eventEnoughAmmoForFiring(enoughAmmo);
        }
    }

    // UpdateGUIEventHandler

    public void CallEventUpdateAmmoGUI(int ammo, int maxAmmo) {
        if(eventUpdateAmmoGUI != null) {
            eventUpdateAmmoGUI(ammo, maxAmmo);
        }
        References.PlayerMaster.CallEventUpdateAmmoGUI(ammo, maxAmmo);
    }

    public void CallEventUpdateWeaponNameGUI(string weaponName) {
        if (eventUpdateWeaponNameGUI != null) {
            eventUpdateWeaponNameGUI(weaponName);
        }
        References.PlayerMaster.CallEventUpdateWeaponNameGUI(weaponName);
    }
}
