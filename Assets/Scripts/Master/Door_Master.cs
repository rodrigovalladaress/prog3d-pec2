﻿using UnityEngine;
/// <summary>
/// Gestión de los eventos relacionados con una puerta.
/// </summary>
public class Door_Master : MonoBehaviour {

    public delegate void GeneralEventHandler();
    public event GeneralEventHandler eventDestroy;
    public event GeneralEventHandler eventOpenDoor;
    public event GeneralEventHandler eventTryToOpenLockedDoor;
    public event GeneralEventHandler eventCloseDoor;
    public event GeneralEventHandler eventUnlockDoor;
    public event GeneralEventHandler eventFinishedMovingDoor;

    public delegate void LockedEventHandler(bool isLocked);
    public event LockedEventHandler eventDoorIsLocked;

    public delegate void TryToUnlockEventHandler(Item_Key.KeyType keyType);
    public event TryToUnlockEventHandler eventCheckIfPlayerCanUnlockDoor;

    public void CallEventDestroy() {
        if(eventDestroy != null) {
            eventDestroy();
        }
    }

    public void CallEventOpenDoor() {
        if (eventOpenDoor != null) {
            eventOpenDoor();
        }
    }

    public void CallEventTryToOpenLockedDoor() {
        if (eventTryToOpenLockedDoor != null) {
            eventTryToOpenLockedDoor();
        }
    }

    public void CallEventDoorClose() {
        if (eventCloseDoor != null) {
            eventCloseDoor();
        }
    }

    public void CallEventUnlockDoor() {
        if(eventUnlockDoor != null) {
            eventUnlockDoor();
        }
    }

    public void CallEventFinishedMovingDoor() {
        if(eventFinishedMovingDoor != null) {
            eventFinishedMovingDoor();
        }
    }

    public void CallEventDoorIsLocked(bool isLocked) {
        if(eventDoorIsLocked != null) {
            //Debug.Log("Door_master isLocked = " + isLocked);
            eventDoorIsLocked(isLocked);
        }
    }

    public void CallEventCheckIfPlayerCanUnlockDoor(Item_Key.KeyType keyType) {
        if(eventCheckIfPlayerCanUnlockDoor!= null) {
            eventCheckIfPlayerCanUnlockDoor(keyType);
        }
        References.PlayerMaster.CallEventTryToUnlockDoor(keyType, this);
    }

}
