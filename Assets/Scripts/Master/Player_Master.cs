﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Gestión de los eventos relacionados con el jugador.
/// </summary>
public class Player_Master : MonoBehaviour {

    [SerializeField]
    private Weapon_Master weaponMaster;

    public delegate void GeneralEventHandler();
    public event GeneralEventHandler eventDied;
    public event GeneralEventHandler eventGameCompleted;
    public event GeneralEventHandler eventFire;
    public event GeneralEventHandler eventUpdateGameOverGUI;
    public event GeneralEventHandler eventUpdateGameCompletedGUI;

    public delegate void ChangeWeaponEventHandler(Weapon_Info.WeaponIdEnum weapon);
    public event ChangeWeaponEventHandler eventPlayerChangeWeapon;

    public delegate void HealthEventHandler(int amount);
    public event HealthEventHandler eventDamaged;
    public event HealthEventHandler eventUpdateHealthGUI;
    public event HealthEventHandler eventUpdateShieldGUI;
    public event HealthEventHandler eventHealthItemObtained;
    public event HealthEventHandler eventShieldItemObtained;

    public delegate void ItemAmmoEventHandler(Weapon_Info.WeaponIdEnum weaponId, int deltaAmmo);
    public event ItemAmmoEventHandler eventAmmoItemObtained;

    public delegate void KeyEventHandler(Item_Key.KeyType keyType);
    public event KeyEventHandler eventKeyItemObtained;

    public delegate void AmmoEventHandler(int ammo);
    public event AmmoEventHandler eventAmmoChanged;

    public delegate void SuccesfullyChangedWeaponEventHandler(Weapon_Info weaponInfo);
    public event SuccesfullyChangedWeaponEventHandler eventSuccesfullyChangedWeapon;

    public delegate void UpdateAmmoGUIEventHandler(int ammo, int maxAmmo);
    public event UpdateAmmoGUIEventHandler eventUpdateAmmoGUI;

    public delegate void UpdateWeaponNameGUIEventHandler(string weaponName);
    public event UpdateWeaponNameGUIEventHandler eventUpdateWeaponNameGUI;

    public delegate void UpdateKeysInfoGUIEventHandler(Dictionary<Item_Key.KeyType, int> keys);
    public event UpdateKeysInfoGUIEventHandler eventUpdateKeysInfoGUI;

    public delegate void UnlockDoorEventHandler(Item_Key.KeyType keyType, Door_Master doorMaster);
    public event UnlockDoorEventHandler eventTryToUnlockDoor;

    // GeneralEventHandler

    public void CallEventDied() {
        if(eventDied != null) {
            eventDied();
        }
    }

    public void CallEventGameCompleted() {
        if(eventGameCompleted != null) {
            eventGameCompleted();
        }
    }

    public void CallEventWeaponFire() {
        if (eventFire != null) {
            eventFire();
        }
        weaponMaster.CallEventRequestWeaponFire();
    }

    public void CallEventUpdateGameOverGUI() {
        if(eventUpdateGameOverGUI != null) {
            eventUpdateGameOverGUI();
        }
    }

    public void CallEventUpdateGameCompletedGUI() {
        if(eventGameCompleted != null) {
            eventUpdateGameCompletedGUI();
        }
    }

    // ChangeWeaponEventHandler

    public void CallEventChangeWeapon(Weapon_Info.WeaponIdEnum weapon) {
        if(eventPlayerChangeWeapon != null) {
            eventPlayerChangeWeapon(weapon);
        }
        weaponMaster.CallEventChangeWeapon(weapon);
    }

    // HealthEventHandler

    public void CallEventDamaged(int damage) {
        if(eventDamaged != null) {
            eventDamaged(damage);
        }
    }

    public void CallEventUpdateHealthGUI(int health) {
        if (eventUpdateHealthGUI != null) {
            eventUpdateHealthGUI(health);
        }
    }

    public void CallEventUpdateShieldGUI(int shield) {
        if (eventUpdateShieldGUI != null) {
            eventUpdateShieldGUI(shield);
        }
    }

    public void CallEventShieldItemObtained(int deltaShield) {
        if (eventShieldItemObtained != null) {
            eventShieldItemObtained(deltaShield);
        }
    }

    public void CallEventHealthItemObtained(int deltaHealth) {
        if (eventHealthItemObtained != null) {
            eventHealthItemObtained(deltaHealth);
        }
    }

    // ItemAmmoEventHandler
    public void CallEventAmmoItemObtained(Weapon_Info.WeaponIdEnum weaponId, int ammo) {
        if(eventAmmoItemObtained != null) {
            eventAmmoItemObtained(weaponId, ammo);
        }
        weaponMaster.CallEventAmmoItemObtained(weaponId, ammo);
    }

    // ItemKeyEventHandler

    public void CallEventKeyItemObtained(Item_Key.KeyType keyType) {
        if(eventKeyItemObtained != null) {
            eventKeyItemObtained(keyType);
        }
    }

      

    // AmmoEventHandler
    public void CallEventAmmoChanged(int ammo) {
        if (eventAmmoChanged != null) {
            eventAmmoChanged(ammo);
        }
    }

    // SuccesfullyChangedWeaponEventHandler
    public void CallEventSuccesfullyChangedWeapon(Weapon_Info weaponInfo) {
        if (eventSuccesfullyChangedWeapon != null) {
            eventSuccesfullyChangedWeapon(weaponInfo);
        }

    }

    // UpdateGUIEventHandler

    public void CallEventUpdateAmmoGUI(int ammo, int maxAmmo) {
        if(eventUpdateAmmoGUI != null) {
            eventUpdateAmmoGUI(ammo, maxAmmo);
        }
    }

    // UpdateWeaponNameGUIEventHandler

    public void CallEventUpdateWeaponNameGUI(string weaponName) {
        if(eventUpdateWeaponNameGUI != null) {
            eventUpdateWeaponNameGUI(weaponName);
        }
    }

    // UpdateKeysInfoGUIEventHandler

    public void CallEventUpdateKeysInfoGUI(Dictionary<Item_Key.KeyType, int> keys) {
        if(eventUpdateKeysInfoGUI != null) {
            eventUpdateKeysInfoGUI(keys);
        }
    }

    // UnlockDoorEventHandler

    public void CallEventTryToUnlockDoor(Item_Key.KeyType keyType, Door_Master doorMaster) {
        if (eventTryToUnlockDoor != null) {
            eventTryToUnlockDoor(keyType, doorMaster);
        }
    }

}
