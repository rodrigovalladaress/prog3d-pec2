﻿using UnityEngine;
/// <summary>
/// Gestión de los eventos relacionados con los ítems.
/// </summary>
public class Item_Master : MonoBehaviour {

    public delegate void GeneralEventHandler();
    public event GeneralEventHandler eventItemObtained;

    public delegate void HealthEventHandler(int delta);

    public delegate void AmmoEventHandler(int weaponId, int deltaAmmo);

    public delegate void KeyEventHandler(Item_Key.KeyType keyType);

    // GeneralEventHandler
    public void CallEventItemObtained() {
        if(eventItemObtained != null) {
            eventItemObtained();
        }
    }

    // HealthEventHandler

    public void CallEventShieldItemObtained(int deltaShield) {
        References.PlayerMaster.CallEventShieldItemObtained(deltaShield);
    }

    public void CallEventHealthItemObtained(int deltaHealth) {
        References.PlayerMaster.CallEventHealthItemObtained(deltaHealth);
    }

    // AmmoEventHandler

    public void CallEventAmmoItemObtained(Weapon_Info.WeaponIdEnum weaponId, int deltaAmmo) {
        References.PlayerMaster.CallEventAmmoItemObtained(weaponId, deltaAmmo);
    }

    // KeyEventHandler

    public void CallEventKeyItemObtained(Item_Key.KeyType keyType) {
        References.PlayerMaster.CallEventKeyItemObtained(keyType);
    }
}
