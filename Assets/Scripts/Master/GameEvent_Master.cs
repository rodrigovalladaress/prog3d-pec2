﻿using UnityEngine;
/// <summary>
/// Gestión de los eventos relacionados con los eventos de juego.
/// </summary>
public class GameEvent_Master : MonoBehaviour {

    public delegate void GeneralEventHandler();
    public event GeneralEventHandler eventCompleted;
    public event GeneralEventHandler eventEnemyDestroyed;

    public void CallEventCompleted() {
        if(eventCompleted != null) {
            eventCompleted();
        }
    }

    public void CallEventEnemyDestroyed() {
        if(eventEnemyDestroyed != null) {
            eventEnemyDestroyed();
        }
    }
	
}
